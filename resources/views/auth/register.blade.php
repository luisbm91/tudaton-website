@extends('layouts.app')

@section('title', 'Tudaton.com')

@section('content')
<section class="mt-40px mb-40px">
	<div class="container">
		<div class="row justify-content-center">
      <div class="col-12 col-sm-8 col-md-6 col-lg-4">
        <div id="alert-form" class="alert alert-danger d-none" role="alert">
        </div>
        <div class="card signinbox">
          <div class="card-body">
            <div class="mt-2 mb-4">
              <a class="btn btn-lg btn-facebook btn-block"
                href="{{ url('login/facebook') }}">
                <i class="fab fa-facebook-f"></i> &nbsp;
                Registrate con Facebook
              </a>
            </div>
            <p class="text-center mb-4">o registrate manualmente</p>
            <form id="form-register"
              data-url="{{ route('register') }}"
              novalidate>
              @csrf
              <div class="form-group">
                <input type="text"
                  class="form-control"
                  name="name"
                  placeholder="Nombres"
                  maxlength="100"
                  required
                  autofocus>
                <div class="invalid-feedback">
                  Debes ingresar tu Nombre
                </div>
              </div>
              <div class="form-group">
                <input type="email"
                  class="form-control"
                  name="email"
                  placeholder="Email"
                  maxlength="100"
                  required>
                <div class="invalid-feedback">
                  Debes ingresar tu Email
                </div>
              </div>
              <div class="form-group">
                <input type="password"
                  class="form-control"
                  name="password"
                  placeholder="Contraseña"
                  minlength="8"
                  maxlength="18"
                  required>
                <div class="invalid-feedback">
                  Debes ingresar una contraseña válida
                </div>
              </div>
              <div class="form-group">
                <input type="password"
                  class="form-control"
                  name="password_confirmation"
                  placeholder="Repetir Contraseña"
                  minlength="8"
                  maxlength="18"
                  required>
                <div class="invalid-feedback">
                  Debes repetir la Contraseña
                </div>
              </div>
              <div class="form-group">
                <button class="btn btn-lg btn-primary btn-block mt-5"
                  type="submit" disabled>
                  Crear Cuenta
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
		</div>
	</div>
</section>
<footer class="footer">
</footer>
@endsection

@push('scripts')
<script>
let $form;

function enableUI() {
  $form.find('button').removeAttr('disabled');
}

function disableUI() {
  $form.find('button').attr('disabled', true);
}

function handleForm($form) {
  let $alert = $('#alert-form');

  disableUI();
  $('.loader').removeClass('d-none');

  $form.find('.form-control').removeClass('is-invalid');
  $alert.addClass('d-none');

  $.ajax({
    method: 'POST',
    url: $form.data('url'),
    data: $form.serialize(),
    dataType: 'json',
  })
  .done(function(resp) {
    if (resp.success) {
      location.href = resp.url;
    }
    else {
      $alert.removeClass('d-none')
      .text(resp.message);
    }
  })
  .fail(function(resp) {
		$alert
		.removeClass('alert-success')
		.addClass('alert-danger')
		.removeClass('d-none')
		.text('Asegurese de rellenar todos los campos correctamente');

    let errors = resp.responseJSON.errors;

    $.each(errors, function(key, message) {
      $form.find(".form-control[name='"+key+"']")
        .addClass('is-invalid')
        .next()
        .text(message);
    });

    $form.addClass('needs-validation');
  })
  .always(function() {
    enableUI();
    $('.loader').addClass('d-none');
  });
}

$(document).ready(function() {
  $form = $('#form-register');
  enableUI();
  
  $form.submit(function(e) {
    e.preventDefault();
    e.stopPropagation();
    handleForm($(this));
  });
});
</script>
@endpush