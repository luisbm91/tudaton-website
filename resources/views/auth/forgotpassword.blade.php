@extends('layouts.app')

@section('title', 'Tudaton.com')

@section('content')
<section class="mt-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="card fgotpasswdbox">
				<div class="card-body">
					<h5 class="card-title text-center mb-5">Resetear Contraseña</h5>
					@if (session('status'))
						<div class="alert alert-success" role="alert">
							{{ session('status') }}
						</div>
					@endif
					<form method="POST" action="{{ route('password.email') }}">
						@csrf
						<div class="form-group">
						    <input type="email"
							    class="form-control @error('email') is-invalid @enderror"
							    name="email"
							    value="{{ old('email') }}" 
							    placeholder="Ingresa tu email"
							    maxlength="80" 
							    required
							    autofocus>
							@error('email')
							    <div class="invalid-feedback">
							    	{{ $message }}
							    </div>
						    @enderror
						</div>
						<div class="form-group">
							<button class="btn btn-lg btn-primary btn-block mt-4"
								type="submit">Resetear</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection