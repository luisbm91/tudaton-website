@extends('layouts.app', ['isHome' => true])

@section('title', 'Tudatazo.com')

@section('content')
<section class="sec-banner"
    style="background-image: url({{ url('img/banner.jpg') }})">
    <div class="bgblack"></div>
    <div class="jumbotron">
        <div class="container">
            <h1 class="jumbotron-heading">
                Todo lo que necesites a un solo click!
            </h1>
            <p class="lead">
                Compra - Vende - Busca - Cambia - Renta
            </p>
            <div class="row justify-content-md-center form-search-hor">
                <div class="col-lg-11 col-12">
                    <form method="GET" action="{{ route('listings') }}">
                        <div class="row">
                            <div class="col-lg-3 col-12 mb-2 pr-0">
                                <div id="form-search-place"
                                    class="form-control-search">
                                    <img src="{{ url('img/flags/'.$country->id.'.png') }}" alt="">
                                    Dónde quieres buscar?
                                </div>
                            </div>
                            <div class="col-lg-3 col-12 mb-2 pr-0 pl-0">
                                <div id="form-search-category"
                                    class="form-control-search">
                                    <i class="fas fa-tags"></i> &nbsp;
                                    Qué categoría buscas?
                                </div>
                            </div>
                            <div class="col-lg-4 col-12 mb-2 pr-0 pl-0">
                                <input type="text"
                                    class="form-control form-control-lg"
                                    name="q"
                                    placeholder="Escribe el producto que buscas"
                                    autocomplete="off" />
                            </div>
                            <div class="col-lg-2 col-12 mb-2 pl-0">
                                <button class="btn btn-block btn-lg btn-primary"
                                    type="submit">
                                    <i class="fas fa-search"></i> &nbsp;
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="hsection mt-70px mb-70px">
    <div class="container">
        <h2 class="text-center">Anuncios Destacados</h2>
        <div class="row">
        @foreach($arrListings as $row)
            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                <div class="card listing">
                    <a href="{{ route('ad.detail', [ 'id' => $row['id'] ]) }}">
                        <img class="card-img-top"
                            src="{{ $row['image'] }}"
                            alt="{{ $row['title'] }}">
                    </a>
                    <div class="card-body">
                        <div class="category">
                            <i class="fas fa-tags"></i> &nbsp;
                            {{ $row['category'] }}
                        </div>
                        <h5 class="title mt-2">
                            <a href="{{ route('ad.detail', [ 'id' => $row['id'] ]) }}">
                                {{ $row['title'] }}
                            </a>
                            <span class="badge badge-secondary">Nuevo</span>
                        </h5>
                        <div class="time mt-3">
                            <i class="far fa-clock"></i> &nbsp;
                            {{ $row['date'] }}
                        </div>
                        <div class="place">
                            <i class="fas fa-map-marker-alt"></i> &nbsp;
                            Miraflores, Lima
                        </div>
                        <div class="price mt-2">
                            <div class="row">
                                <div class="col-8">
                                    {{ $row['currency'] }}
                                    {{ $row['price'] }}
                                    <span>por mes</span>
                                </div>
                                <div class="col-4 d-flex justify-content-end">
                                    <button class="btn">
                                        <i class="fas fa-heart"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</section>
<section class="sec-stores hsection">
    <div class="container">
        <h2 class="text-center">Las mejores tiendas te esperan</h2>
        <div class="row">
        @foreach($stores as $row)
            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                <div class="card storebox">
                    <img class="card-img-top"
                        src="{{ url('img/no-store-banner.jpg') }}"
                        alt="store banner" />
                    <a href="#" class="brand">
                        <img class="card-img-top"
                            src="{{ url('img/no-store-brand.jpg') }}"
                            alt="store banner" />
                    </a>
                    <div class="card-body">
                        <h5 class="title">
                            <a href="#">
                                {{ $row->name }}
                            </a>
                        </h5>
                        <div class="products">
                            <div class="row">
                                <div class="col">
                                    <a href="#">
                                        <img src="{{ url('img/no-store-image.jpg') }}"
                                            alt="store product image" />
                                    </a>
                                </div>
                                <div class="col">
                                    <a href="#">
                                        <img src="{{ url('img/no-store-image.jpg') }}"
                                            alt="store product image" />
                                    </a>
                                </div>
                                <div class="col">
                                    <a href="#">
                                        <img src="{{ url('img/no-store-image.jpg') }}"
                                            alt="store product image" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <a href="#">Ver Tienda</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</section>
<footer class="footer">

</footer>
@endsection

@section('modal')
<div class="modal modal-pr-place" id="modal-search-place"
    tabindex="-1" role="dialog" aria-hidden="true"
    data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <img src="{{ url('img/flags/'.$country->id.'.png') }}"
                        alt="{{ $country->name }}">
                    {{ $country->name }}
                    &nbsp;
                    &nbsp;
                    <button class="btn btn-info btn-sm"
                        id="btn-change-country">Cambiar</button>
                    <button class="btn btn-warning btn-sm d-none"
                        id="btn-return-country">Regresar</button>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modal-search-place-countries" class="d-none">
                    <ul class="countries">
                    @foreach($countries as $row)
                        <li>
                            <a href="javascript:void(0)"
                                data-url="{{ url()->current().'/'.strtolower($row->code) }}">
                                <img src="{{ url('img/flags/'.$row->id.'.png') }}"
                                    alt="{{ $row->name }}">
                                &nbsp; &nbsp;
                                {{ $row->name }}
                            </a>
                        </li>
                    @endforeach
                    </ul>
                </div>
                <div id="modal-search-place-places">
                    <!--
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control form-control-lg"
                            placeholder="Escribe donde quieres buscar" />
                        </div>
                    </div>
                    <p id="place-subtitle-info" class="mt-4">
                        O puedes seleccionar
                    </p>
                    -->
                    <h3 id="place-subtitle-name" class="d-none">
                    </h3>
                    <ul class="places"
                        data-url="{{ route('get.places') }}">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-pr-place" id="modal-search-category"
    tabindex="-1" role="dialog" aria-hidden="true"
    data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Categorias
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 id="category-subtitle-name" class="d-none">
                </h3>
                <ul class="places"
                    data-url="{{ route('get.categories') }}">
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
function openPlaces(id) {
    let $box    = $('#modal-search-place-places');
    let url     = $box.find('ul').data('url');
    let name    = $box.data('name');
    let csrf    = $("meta[name='csrf-token']").attr('content');
    
    $.post(url,
    { _token : csrf, id : id },
    function(resp) {
        /*
        $('#place-subtitle-name').removeClass('d-none')
        .text(name);
        */

        let onclick = '';
        let content = '';
        $.each(resp, function(idx, val) {
            if (val.hasc == 0) {
                onclick = `searchPlace(${val.id})`;
            }
            else {
                onclick = `openPlaces(${val.id})`;
            }

            content += `<li data-name="${val.name}">
                <div class="row">
                    <div class="col-9">
                        <a href="javascript:void(0)"
                            onclick="${onclick}">
                            <i class="fas fa-angle-right"></i>&nbsp;
                            ${val.name}
                        </a>
                    </div>
                    <div class="col-3 text-right">
                        <button class="btn btn-sm btn-info"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Buscar"
                            onclick="searchPlace(${val.id})">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </li>`;
        });

        $box.find('ul').html(content);
    }, 'json');
}

function searchPlace(id) {
    alert('ok '+id);
}

function openCategories() {
    let $box    = $('#modal-search-category');
    let url     = $box.find('ul').data('url');
    let name    = $box.data('name');
    let csrf    = $("meta[name='csrf-token']").attr('content');
    
    $.post(url,
    { _token : csrf },
    function(resp) {
        let content = '';
        $.each(resp, function(idx, val) {
            content += `<li data-name="${val.name}">
                <div class="row">
                    <div class="col-9">
                        <a href="javascript:void(0)"
                            onclick="searchCategory(${val.id})">
                            <i class="fas fa-angle-right"></i>&nbsp;
                            ${val.name}
                        </a>
                    </div>
                    <div class="col-3 text-right">
                        <button class="btn btn-sm btn-info"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Buscar"
                            onclick="searchCategory(${val.id})">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </li>`;
        });

        $box.find('ul').html(content);
    }, 'json');
}

$(document).ready(function() {
    window.addEventListener('scroll', function() {
        const navbar = document.querySelector('.navbar-web');
        navbar.classList.toggle('nav-sticky', window.scrollY > 60);
    });

    $('#form-search-place').click(function() {
        openPlaces({{ $country->id }});
        $('#modal-search-place').modal('show');
    });

    $('#form-search-category').click(function() {
        openCategories();
        $('#modal-search-category').modal('show');
    });

    $('#btn-change-country').click(function() {
        $('#modal-search-place-countries').removeClass('d-none');
        $('#modal-search-place-places').addClass('d-none');
        $('#btn-change-country').addClass('d-none');
        $('#btn-return-country').removeClass('d-none');
    });
    
    $('#btn-return-country').click(function() {
        $('#modal-search-place-countries').addClass('d-none');
        $('#modal-search-place-places').removeClass('d-none');
        $('#btn-change-country').removeClass('d-none');
        $('#btn-return-country').addClass('d-none');
    });

    $('#modal-search-place-countries')
    .find('a').click(function() {
        let url = $(this).data('url');
        location.href = url;
    });

});
</script>
@endpush