@extends('layouts.app')

@section('title', 'Tudatazo.com')

@section('content')
<section class="sec-banner banner-inside"
    style="background-image: url({{ url('img/banner.jpg') }})">
    <div class="bgblack"></div>
    <div class="jumbotron">
        <div class="container">
            <h1 class="jumbotron-heading">
                Anuncios
            </h1>
            <p class="lead">
                Inico &nbsp;
                <i class="fas fa-angle-right"></i> &nbsp;
                Anuncios
            </p>
        </div>
    </div>
</section>
<section class="sec-search-hor mt-40px">
    <div class="container">
        <div class="row form-search-hor">
            <div class="col-12">
                <form method="GET" action="{{ route('listings') }}"
                    id="form-search">
                    <div class="row">
                        <div class="col-lg-3 col-12 mb-2 pr-0">
                            <div id="form-search-category"
                                class="form-control-search">
                                <i class="fas fa-tags"></i> &nbsp;
                                Dónde quieres Buscar?
                            </div>
                        </div>
                        <div class="col-lg-3 col-12 mb-2 pr-0 pl-0">
                            <div id="form-search-category"
                                class="form-control-search">
                                <i class="fas fa-tags"></i> &nbsp;
                                Qué categoría buscas?
                            </div>
                        </div>
                        <div class="col-lg-4 col-12 mb-2 pr-0 pl-0">
                            <input type="text"
                                class="form-control form-control-lg"
                                name="q"
                                value="{{ $request->q }}"
                                placeholder="Escribe el producto que buscas"
                                autocomplete="off" />
                        </div>
                        <div class="col-lg-2 col-12 mb-2 pl-0">
                            <button class="btn btn-block btn-lg btn-primary"
                                type="submit" disabled>
                                <i class="fas fa-search"></i> &nbsp;
                                Buscar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="mt-30px mb-50px">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-3">
                <h3 class="search-left-title">
                    + Filtros
                </h3>
                <div class="card search-left-box">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        Acción
                        <span class="badge badge-primary badge-pill rounded-circle">
                            <i class="fas fa-chevron-up"></i>
                        </span>
                    </div>
                    <ul class="list-group list-group-flush pt-3 pb-2">
                    @foreach($actions as $row)
                        <li class="list-group-item">
                            <div class="form-check">
                                <input class="form-check-input" type="radio"
                                    name="srchAction" id="srchAction-{{ $row->id }}"
                                    value="{{ $row->id }}">
                                <label class="form-check-label"
                                    for="srchAction-{{ $row->id }}">
                                    {{ $row->name }}
                                </label>
                            </div>
                        </li>
                    @endforeach
                    </ul>
                </div>
                <div class="card search-left-box border-top-0">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        Rango de Precios
                        <span class="badge badge-primary badge-pill rounded-circle">
                            <i class="fas fa-chevron-up"></i>
                        </span>
                    </div>
                    <ul class="list-group list-group-flush pt-3 pb-2">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    <input type="password" class="form-control"
                                        name="srchPriceMin" id="srchPriceMin" />
                                </div>
                                <div class="col">
                                    <input type="password" class="form-control"
                                        name="srchPriceMax" id="srchPriceMax" />
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-lg-9">
                <div class="card ordering">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                &nbsp;
                            </div>
                            <div class="col d-flex justify-content-end">
                                <select class="form-control col-md-6 rounded-0" name="orderBy" id="orderBy">
                                    <option value="">Mas visto</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                @foreach($arrListings as $row)
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="card listing">
                            <a href="{{ route('ad.detail', [ 'id' => $row['id'] ]) }}">
                                <img class="card-img-top"
                                    src="{{ $row['image'] }}"
                                    alt="{{ $row['title'] }}">
                            </a>
                            <div class="card-body">
                                <div class="category">
                                    <i class="fas fa-tags"></i> &nbsp;
                                    {{ $row['category'] }}
                                </div>
                                <h5 class="title mt-2">
                                    <a href="{{ route('ad.detail', [ 'id' => $row['id'] ]) }}">
                                        {{ $row['title'] }}
                                    </a>
                                    <span class="badge badge-secondary">Nuevo</span>
                                </h5>
                                <div class="time mt-3">
                                    <i class="far fa-clock"></i> &nbsp;
                                    {{ $row['date'] }}
                                </div>
                                <div class="place">
                                    <i class="fas fa-map-marker-alt"></i> &nbsp;
                                    Miraflores, Lima
                                </div>
                                <div class="price mt-2">
                                    <div class="row">
                                        <div class="col-8">
                                            {{ $row['currency'] }}
                                            {{ $row['price'] }}
                                            <span>por mes</span>
                                        </div>
                                        <div class="col-4 d-flex justify-content-end">
                                            <button class="btn">
                                                <i class="fas fa-heart"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="footer">
</footer>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
    
    let $form = $('#form-search');
    $form.find('button').removeAttr('disabled');

    $form.submit(function(e) {
        console.log('ok');
    });

});
</script>
@endpush