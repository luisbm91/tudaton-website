@extends('layouts.app')

@section('title', 'Tudatazo.com')

@section('content')
<section class="sec-banner banner-inside"
    style="background-image: url({{ url('img/banner.jpg') }})">
    <div class="bgblack"></div>
    <div class="jumbotron">
        <div class="container">
            <h1 class="jumbotron-heading">
                Tiendas
            </h1>
            <p class="lead">
                Inico &nbsp;
                <i class="fas fa-angle-right"></i> &nbsp;
                Tiendas
            </p>
        </div>
    </div>
</section>
<section class="sec-search-hor mt-50px">
    <div class="container">
        <div class="row justify-content-md-center form-search-hor">
            <div class="col-12 col-lg-8">
                <div class="row">
                    <div class="col-lg-4 col-12 mb-2">
                        <div id="form-search-category"
                            class="form-control-search">
                            <i class="fas fa-tags"></i> &nbsp;
                            Qué categoría buscas?
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mb-2">
                        <input type="text"
                            class="form-control form-control-lg"
                            name="key"
                            placeholder="Escribe el producto que buscas" />
                    </div>
                    <div class="col-lg-2 col-12 mb-2">
                        <button class="btn btn-block btn-lg btn-primary"
                            type="button">
                            <i class="fas fa-search"></i> &nbsp;
                            Buscar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sec-stores mt-60px">
    <div class="container">
        <div class="row">
        @foreach($stores as $row)
            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                <div class="card storebox">
                    <img class="card-img-top"
                        src="{{ url('img/no-store-banner.jpg') }}"
                        alt="store banner" />
                    <a href="#" class="brand">
                        <img class="card-img-top"
                            src="{{ url('img/no-store-brand.jpg') }}"
                            alt="store banner" />
                    </a>
                    <div class="card-body">
                        <h5 class="title">
                            <a href="#">
                                {{ $row->name }}
                            </a>
                        </h5>
                        <div class="products">
                            <div class="row">
                                <div class="col">
                                    <a href="#">
                                        <img src="{{ url('img/no-store-image.jpg') }}"
                                            alt="store product image" />
                                    </a>
                                </div>
                                <div class="col">
                                    <a href="#">
                                        <img src="{{ url('img/no-store-image.jpg') }}"
                                            alt="store product image" />
                                    </a>
                                </div>
                                <div class="col">
                                    <a href="#">
                                        <img src="{{ url('img/no-store-image.jpg') }}"
                                            alt="store product image" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <a href="#">Ver Tienda</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</section>

<footer class="footer">
</footer>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
    
});
</script>
@endpush