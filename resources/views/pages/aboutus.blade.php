@extends('layouts.app')

@section('title', 'Tudatazo.com')

@section('content')
<section class="sec-banner banner-inside"
    style="background-image: url({{ url('img/banner.jpg') }})">
    <div class="bgblack"></div>
    <div class="jumbotron">
        <div class="container">
            <h1 class="jumbotron-heading">
                Nosotros
            </h1>
            <p class="lead">
                Inico &nbsp;
                <i class="fas fa-angle-right"></i> &nbsp;
                Nosotros
            </p>
        </div>
    </div>
</section>
<section class="sec-aboutus mt-70px">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h3 class="text-left mb-4">Nosotros</h3>
                <p>
                    Somos un equipo de trabajo con espíritu emprendedor y que busca a través de este sitio web ayudar a que más personas puedan llegar a sus clientes de manera fácil mediante su propia tienda virtual. Al igual que los usuarios a los cuales nos dirigimos, nosotros como empresa encontramos esta oportunidad en el mercado y decidimos a través de nuestras propias experiencias hacer realidad la solución ideal a esta necesidad, ser tu propio jefe sin salir de casa. Esta es tu gran oportunidad de crecer y estaremos contigo para lograr tu objetivo juntos!
                </p>
                <h3 class="text-left mt-5 mb-4">¿Qué Hacemos?</h3>
                <p>
                    Ponemos a tu disposición nuestra plataforma virtual para que seas capaz de crear tu propia tienda virtual y llegar de este modo a muchas personas. Serás capaz de publicar tus anuncios de manera gratuita y poder adquirir nuevos planes accesibles para todos para que tus anuncios sean mejor posicionados y poder publicar las veces que quieras! Lo mejor de todo es que nuestro sitio web está dirigido a varias categorías desde alimentos, ropa, inmobiliaria, etc donde podrás buscar, comprar, vender, alquilar, todo a un solo click! y lo mejor de todo sin comisiones! Estamos felices que seas parte de Tudaton! Disfruta esta nueva experiencia y Unete a nosotros!
                </p>
            </div>
            <div class="col-md-5">
                <img src="{{ url('img/about-us.jpg') }}" class="img-fluid" />
            </div>
        </div>
    </div>
</section>
<footer class="footer">
</footer>
@endsection