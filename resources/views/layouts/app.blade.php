<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>@yield('title')</title>
	<meta name="description" content="Todo lo que necesites a un solo click">
	<meta name="keywords" content="">
	<meta name="author" content="luisbardales.com">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ url('img/favicon.png') }}" type="image/png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" />
	<link rel="stylesheet" type="text/css" href="{{ url('css/style.css') }}">
</head>
<body>
<div class="loader">
    <div class="spinner-border text-warning" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</div>
<header class="header">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left">
                        <i class="fas fa-headset"></i> &nbsp;
                        Atención al Cliente: &nbsp;&nbsp;
                        <span class="email">
                            info@tudaton.com
                        </span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <ul class="nav justify-content-end navbar-top">
                        @guest
                        <li class="nav-item">
                            <a href="{{ route('login') }}"
                                class="nav-link">
                                <i class="fas fa-sign-in-alt"></i> &nbsp;
                                Ingresar
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('register') }}" class="nav-link">
                                <i class="fas fa-user-plus"></i> &nbsp;
                                Registro
                            </a>
                        </li>
                        @else
                        <li class="nav-item">
                            <a href="javascript:void(0)"
                                class="nav-link"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt"></i> &nbsp;
                                Salir
                            </a>
                        </li>
                        @endguest
                        <li class="nav-item">
                            <a href="{{ route('cgl.listing.create') }}"
                            class="btn btn-outline-secondary">
                                <i class="fas fa-store"></i> &nbsp;
                                Publica Aquí
                            </a>
                        </li>
                    </ul>
                    <form id="logout-form" method="POST" hidden
                        action="{{ route('logout') }}">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
	<div class="header-navbar">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-main">
                <a class="navbar-brand" href="/">
                    <img src="{{ url('img/logo.png') }}"
                        height="27" alt="logo tudaton.com" />
                </a>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                      <a href="/" class="nav-link
                      <?= isset($isPageHome) ? 'active' : '' ?>">
                        Inicio
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{ route('listings') }}" class="nav-link
                      <?= isset($isPageListings) ? 'active' : '' ?>">
                        Anuncios
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{ route('stores') }}" class="nav-link
                      <?= isset($isPageStores) ? 'active' : '' ?>">
                        Tiendas
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{ route('aboutus') }}" class="nav-link
                      <?= isset($isPageAboutus) ? 'active' : '' ?>">
                        Nosotros
                      </a>
                    </li>
                </ul>
                <ul class="nav justify-content-end">
                  @auth
                  <li class="nav-item">
                    <a class="nav-link"
                      href="{{ route('cgl.dashboard') }}">
                      <div class="media">
                        @php
                        if (empty(Auth::user()->image))
                          $imageUrl = url('img/avatar.jpg');
                        else
                          $imageUrl = Storage::url(sha1(Auth::user()->id).'/'.Auth::user()->image);
                        @endphp
                        <img class="align-self-center rounded-circle img-thumbnail mr-3"
                          src="{{ $imageUrl }}"
                          alt="{{ Auth::user()->name }}" />
                        <div class="media-body">
                          <span>{{ Auth::user()->name }}</span><br/>
                          <span class="auth-child">ir a mi cuenta</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  @endauth
                </ul>
            </nav>
        </div>
	</div>
</header>
<main>
    @yield('content')
</main>

@section('modal')
@show

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('.loader').addClass('d-none');
  });
</script>
@stack('scripts')
</body>
</html>