<div class="card admin-nav shadow-sm">
  <div class="card-header">
    <div class="media">
      <img src="{{ $avatar }}"
        class="rounded-circle img-thumbnail shadow mr-3"
        alt="{{ $userName }}" />
      <div class="media-body">
        <h6 class="auth-name">
          {{ $userName }}
        </h6>
        <p class="auth-status">
          Estado:
          <span class="badge badge-{{ $statusClass }}">
            {{ $statusName }}
          </span>
        </p>
        <p class="auth-type">
          Cuenta:
          <strong>
            {{ $typeName }}
          </strong>
        </p>
      </div>
    </div>
  </div>
  <ul class="list-group list-group-flush">
    @if ($typeId == 1)
      <li class="list-group-item">
        <a href="{{ route('cgl.dashboard') }}"
          @if ($selected === 'LIST')
            class=active
          @endif
          >
          <i class="fas fa-bullhorn"></i>
          &nbsp; &nbsp;
          Aprobación de anuncios
        </a>
      </li>
    @elseif ($typeId == 2)
      <li class="list-group-item">
        <a href="javascript:void(0)"
          class="link-store"
          onclick="event.preventDefault();
          document.getElementById('form-store-deal').submit();">
          <i class="fas fa-store"></i>
          &nbsp; &nbsp;
          Conviertete en Tienda
        </a>
        <form id="form-store-deal" method="POST" hidden
          action="{{ route('cgl.store.deal') }}">
          @csrf
        </form>
      </li>
      <li class="list-group-item">
        <a href="{{ route('cgl.profile') }}"
          @if ($selected === 'PROF')
            class=active
          @endif
          >
          <i class="far fa-user"></i>
          &nbsp; &nbsp;
          Mi perfil
        </a>
      </li>
      <li class="list-group-item">
        <a href="{{ route('cgl.dashboard') }}"
          @if ($selected === 'LIST')
            class=active
          @endif
          >
          <i class="fas fa-bullhorn"></i>
          &nbsp; &nbsp;
          Mis anuncios
        </a>
      </li>
    @elseif ($typeId == 3)
      <li class="list-group-item">
        <a href="{{ route('cgl.dashboard') }}"
          @if ($selected === 'LIST')
            class=active
          @endif
          >
          <i class="fas fa-bullhorn"></i>
          &nbsp; &nbsp;
          Mis anuncios
        </a>
      </li>
      <li class="list-group-item">
        <a href="{{ route('cgl.store') }}"
          @if ($selected === 'STOR')
            class=active
          @endif
          >
          <i class="fas fa-store"></i>
          &nbsp; &nbsp;
          Mi tienda
        </a>
      </li>
      <li class="list-group-item">
        <a href="{{ route('cgl.profile') }}"
          @if ($selected === 'PROF')
            class=active
          @endif
          >
          <i class="far fa-user"></i>
          &nbsp; &nbsp;
          Mi perfil
        </a>
      </li>
    @else
      <li class="list-group-item">&nbsp;</li>
    @endif
    
    <!--
    <li class="list-group-item">
      <a href="{{ route('cgl.profile') }}"
        @if ($selected === 'MESS')
          class=active
        @endif
        >
        <i class="far fa-comments"></i>
        &nbsp; &nbsp;
        Mis mensajes
      </a>
    </li>
    <li class="list-group-item">
      <a href="{{ route('cgl.profile') }}"
        @if ($selected === 'SHOP')
          class=active
        @endif
        >
        <i class="fas fa-shopping-basket"></i>
        &nbsp; &nbsp;
        Mis compras
      </a>
    </li>
    <li class="list-group-item">
      <a href="{{ route('cgl.profile') }}"
        @if ($selected === 'WHIS')
          class=active
        @endif
        >
        <i class="far fa-heart"></i>
        &nbsp; &nbsp;
        Mis favoritos
      </a>
    </li>
    -->
  </ul>
</div>