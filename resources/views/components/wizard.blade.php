<div class="card admin-wizard">
  <ul class="list-group list-group-flush">
    @foreach($options as $key => $option)
      <li class="list-group-item
        @if($option['selected'])
          active
        @endif
        ">
        <div class="row">
          <div class="col-3">
            @if($option['hasCheck'])
              <i class="fas fa-check"></i>
            @endif
          </div>
          <div class="col-9">
            <a href="{{ $option['route'] }}">
              {{ $key + 1 }}. &nbsp; {{ $option['title'] }}
            </a>
          </div>
        </div>
      </li>
    @endforeach
  </ul>
</div>