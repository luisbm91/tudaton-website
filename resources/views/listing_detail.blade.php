@extends('layouts.app')

@section('title', $listing->title.' - Tudaton.com')

@section('content')
<section class="mt-5">
	<div class="container">
		<div class="row">
			<div class="col-md-8 mb-3">
				<div class="card lstitlebox mb-3">
					<div class="card-body">
						<h1>
							{{ $listing->title }}
						</h1>
						<div class="row">
							<div class="col">
								<!--
								<a id="btn-share-social" class="btn btn-outline-info btn-sm"
									data-toggle="tooltip" data-placement="top"
									title="Compartir en Redes Sociales"
									href="javascript:void(0)">
									<i class="fas fa-share-alt"></i>
									&nbsp; Compartir
								</a>
								<a href="#" class="btn btn-outline-info btn-sm"
									data-toggle="tooltip" data-placement="bottom"
									title="Denunciar Anuncio">
									<i class="far fa-flag"></i>
								</a>
								-->
							</div>
							<div class="col text-right">
								<span class="h5">
									{{ $currency->name }}
									<span class="h4">{{ $listing->price }}</span>
								</span>
							</div>
						</div>
						<div id="carouselPhotos"
							class="carousel slide carousel-fade mt-4"
							data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselPhotos" 
									data-slide-to="0" class="active"></li>
								<li data-target="#carouselPhotos" 
									data-slide-to="1"></li>
								<li data-target="#carouselPhotos" 
									data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner">
									@foreach ($images as $key => $value)
								<div class="carousel-item {{ ($key == 0) ? 'active' : '' }}">
									<img src="{{ $value }}"
										class="d-block w-100" alt="Fotogragia">
								</div>
									@endforeach
							</div>
							<a class="carousel-control-prev"
								href="#carouselPhotos"
								role="button" data-slide="prev">
								<span class="carousel-control-prev-icon"
									aria-hidden="true"></span>
								<span class="sr-only">Anterior</span>
							</a>
							<a class="carousel-control-next"
								href="#carouselPhotos"
								role="button" data-slide="next">
								<span class="carousel-control-next-icon"
									aria-hidden="true"></span>
								<span class="sr-only">Siguiente</span>
							</a>
						</div>
					</div>
				</div>
				<div class="card lsdescripbox">
					<div class="card-body">
						<h5 class="card-title">Descripcion</h5>
						<div class="card-text">
							{{ $listing->title }}
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card lsprofilebox">
					<div class="card-body text-center">
						<img src="{{ $user->image }}"
							class="rounded-circle img-thumbnail shadow"
							alt="{{ $user->name }}">
						<h5 class="mt-4">{{ $user->name }}</h5>
						<a class="btn btn-outline-info btn-sm mt-3"
							href="#">
							<i class="fas fa-user"></i>
							&nbsp; Ver perfil Completo
						</a>
						<div class="row mt-4 phone">
							<div class="col-12 text-center">
                                <a href="tel:{{ $user->phone }}">
								    <i class="fas fa-phone-alt"></i>
                                </a>
                                &nbsp; &nbsp;
                                <a href="tel:{{ $user->phone }}">
                                    {{ $user->phone }}
                                </a>
							</div>
						</div>
						<div class="row mt-4">
							<div class="col text-left">
								<span>Contáctame:</span>
							</div>
						</div>
						<div class="row mt-4">
							<div class="col text-left">
								@if ($userDetail->whatsapp)
									<a class="btn btn-light shadow-sm"
										data-toggle="tooltip" data-placement="bottom"
										title="Whatsapp" target="_blank"
										style="color:#46A049;" 
										href="https://wa.me/{{ $userDetail->whatsapp }}?text=Hola%20{{ $user->name }}">
										<i class="fab fa-whatsapp"></i>
									</a>
								@endif
								@if ($userDetail->skype)
									<a class="btn btn-light shadow-sm"
										data-toggle="tooltip" data-placement="bottom"
										title="Skype" target="_blank"
										style="color:#00aff0;" 
										href="skype:{{ $userDetail->skype }}?call">
										<i class="fab fa-skype"></i>
									</a>
								@endif
								@if ($userDetail->facebook)
									<a class="btn btn-light shadow-sm"
										data-toggle="tooltip" data-placement="bottom"
										title="Facebook" target="_blank"
										style="color:#3B5998;"
										href="{{ $userDetail->facebook }}">
										<i class="fab fa-facebook-f"></i>
									</a>
								@endif
								@if ($userDetail->instagram)
									<a class="btn btn-light shadow-sm"
										data-toggle="tooltip" data-placement="bottom"
										title="Instagram" target="_blank"
										style="color:#C90082;"
										href="{{ $userDetail->instagram }}">
										<i class="fab fa-instagram"></i>
									</a>
								@endif
								@if ($userDetail->twitter)
									<a class="btn btn-light shadow-sm"
										data-toggle="tooltip" data-placement="bottom"
										title="Twitter" target="_blank"
										style="color:#55ACEE;"
										href="{{ $userDetail->twitter }}">
										<i class="fab fa-twitter"></i>
									</a>
								@endif
								@if ($userDetail->youtube)
									<a class="btn btn-light shadow-sm"
										data-toggle="tooltip" data-placement="bottom"
										title="Youtube" target="_blank"
										style="color:#bb0000;"
										href="{{ $userDetail->youtube }}">
										<i class="fab fa-youtube"></i>
									</a>
								@endif
								@if ($userDetail->pinterest)
									<a class="btn btn-light shadow-sm"
										data-toggle="tooltip" data-placement="bottom"
										title="Pinterest" target="_blank"
										style="color:#d73532;"
										href="{{ $userDetail->pinterest }}">
										<i class="fab fa-pinterest-p"></i>
									</a>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div id="box-share-social" class="share-social">
    <a href="https://www.facebook.com/sharer/sharer.php?u=<?= url()->full() ?>"
        target="_blank" class="btn btn-lg btn-facebook"
        data-toggle="tooltip" data-placement="right"
        title="Compartir en Facebook">
        <i class="fab fa-facebook-f"></i>
    </a>
    <a href="https://api.whatsapp.com/send?text=<?= url()->full() ?>"
        target="_blank" class="btn btn-lg btn-whatsapp"
        data-toggle="tooltip" data-placement="right"
        title="Compartir en Whatsapp">
        <i class="fab fa-whatsapp"></i>
    </a>
    <a href="https://twitter.com/home?status=<?= url()->full() ?>"
        target="_blank" class="btn btn-lg btn-twitter"
        data-toggle="tooltip" data-placement="right"
        title="Compartir en Twitter">
        <i class="fab fa-twitter"></i>
    </a>
    <a href="https://pinterest.com/pin/create/button/?url=<?= url()->full() ?>"
        target="_blank" class="btn btn-lg btn-pinterest"
        data-toggle="tooltip" data-placement="right"
        title="Compartir en Pinterest">
        <i class="fab fa-pinterest-p"></i>
    </a>
    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= url()->full() ?>"
        target="_blank" class="btn btn-lg btn-linkedin"
        data-toggle="tooltip" data-placement="right"
        title="Compartir en Linkedin">
        <i class="fab fa-linkedin-in"></i>
    </a>
</div>
@endsection