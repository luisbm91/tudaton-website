@extends('layouts.app')

@section('title', 'Tudaton.com')

@section('content')
<section class="mt-40px mb-50px">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-4">
        <x-admin-sidebar selected="PROF" />
			</div>
			<div class="col-lg-9">
        
            <h2 class="text-center mb-50px font-ubuntu">
              Elige uno de nuestros Planes
            </h2>

            <div class="row">
              <div class="col-md-4 col-sm-6">
                  <div class="pricingTable">
                      <div class="pricingTable-header">
                        <h3 class="title">Ahorrador</h3>
                      </div>
                      <div class="price-value">
                        <span class="currency">S/</span>
                        <span class="amount">25</span><br/>
                        <span class="duration">mensual</span>
                      </div>
                      <ul class="pricing-content">
                        <li>5 Prodcucto en linea</li>
                        <li>Duración 3 meses</li>
                        <li>Carrito de compras</li>
                      </ul>
                      <div class="pricingTable-signup">
                        <a href="#">Adquirir</a>
                      </div>
                  </div>
              </div>
              <div class="col-md-4 col-sm-6">
                  <div class="pricingTable purple">
                      <div class="pricingTable-header">
                          <h3 class="title">Emprendedor</h3>
                      </div>
                      <div class="price-value">
                          <span class="currency">S/</span>
                          <span class="amount">65</span><br/>
                          <span class="duration">mensual</span>
                      </div>
                      <ul class="pricing-content">
                        <li>15 Prodcucto en linea</li>
                        <li>Duración 6 meses</li>
                        <li>Carrito de compras</li>
                      </ul>
                      <div class="pricingTable-signup">
                          <a href="#">Adquirir</a>
                      </div>
                  </div>
              </div>
              <div class="col-md-4 col-sm-6">
                  <div class="pricingTable blue">
                      <div class="pricingTable-header">
                          <h3 class="title">Negocios</h3>
                      </div>
                      <div class="price-value">
                          <span class="currency">S/</span>
                          <span class="amount">100</span><br/>
                          <span class="duration">mensual</span>
                      </div>
                      <ul class="pricing-content">
                        <li>25 Prodcucto en linea</li>
                        <li>Duración 6 meses</li>
                        <li>Carrito de compras</li>
                      </ul>
                      <div class="pricingTable-signup">
                          <a href="#">Adquirir</a>
                      </div>
                  </div>
              </div>
            </div>


			</div>
		</div>
	</div>
</section>
<footer class="footer">
</footer>
@endsection

@push('scripts')
<script>
function changeStatus($button) {
    $('.loader').removeClass('d-none');

    $.ajax({
        url: $button.data('url'),
        type: 'POST',
        data: {
            _token: $("meta[name='csrf-token']").attr('content'),
            id: $button.data('id')
        },
        dataType: 'json',
    })
    .always(function() {
        $('.loader').addClass('d-none');
        location.reload();
    });
}

$(document).ready(function() {
    $('.ls-action').click(function() {
        changeStatus($(this));
    });
});
</script>
@endpush