@extends('layouts.app')

@section('title', 'Tudaton.com')

@section('content')
<section class="mt-40px mb-50px">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-4">
        <x-admin-sidebar selected="PROF" />
			</div>
			<div class="col-lg-9">
				<form id="form-profile"
					data-url="{{ route('cgl.profile.edit') }}"
					novalidate>
					@csrf
					<div id="alert-form" class="alert mb-30px d-none" role="alert">
					</div>
					<div class="row mt-3 mb-40px admin-title">
						<div class="col-12">
							<h4 class="pl-3">
								<i class="far fa-user"></i> &nbsp;
								Mi Perfil
							</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-4">
							<div id="alert-upload"
								class="alert alert-danger d-none"
								role="alert">
								&nbsp;
							</div>
							<div class="card m-auto admin-avatar">
								@php
								if (empty($user->image)) {
									$imageUrl = url('img/avatar.jpg');
								}
								else {
									$imageUrl = Storage::url(sha1($user->id).'/'.$user->image);
								}
								@endphp
								<label for="avatar-upload"
									class="m-0">
									<img src="{{ $imageUrl }}"
										class="card-img-top img-thumbnail"
										alt="Foto de perfil" />
									<div class="card-img-overlay">
										<p class="card-text text-center">
											<i class="fas fa-camera"></i><br>
											Subir Imagen
										</p>
									</div>
								</label>
								<input type="file" id="avatar-upload"
									accept="image/x-png,image/jpeg"
									data-url="{{ route('cgl.avatar') }}" hidden />
							</div>
							<hr />
							<div class="card shadow-sm mb-50px">
								<div class="card-body">
									<div class="form-group">
										<label>
											Creado desde:
										</label>
										<p class="font-weight-bold">
											{{ date('j \d\e F Y', strtotime($user->created_at)) }}
										</p>
									</div>
									<div class="form-group">
										<label>
											Ultima actualización:
										</label>
										<p class="font-weight-bold">
											{{ date('j \d\e F Y', strtotime($user->updated_at)) }}
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-8">
							<div class="card shadow-sm mb-50px">
								<div class="card-body">
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">
											Nombre
										</label>
										<div class="col-sm-9">
											<input type="text" name="name"
												class="form-control"
												value="{{ $user->name }}"
												maxlength="100"
												required />
											<div class="invalid-feedback">
												Debes ingresar tu Nombre
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">
											Celular
										</label>
										<div class="col-sm-9">
											<input type="tel" name="phone"
												class="form-control"
												value="{{ $user->phone }}"
												maxlength="15"
												placeholder="Ejemplo: 123 456 789"
												required />
											<div class="invalid-feedback">
												Ingresa un Celular Válido
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row mt-3 mb-3 admin-title">
								<div class="col-12">
									<h5 class="pl-3">
										Define tu perfil
									</h5>
								</div>
							</div>
							<div class="card shadow-sm mb-50px">
								<div class="card-body">
									<div class="form-group mb-0">
										<textarea class="form-control"
											name="description"
											placeholder="Ingresa una descripción de tu persona para que te conozcan"
											rows="8"
											maxlength="1000">{{ $udet->description }}</textarea>
										<div class="invalid-feedback">
											Debes ingresar tu Nombre
										</div>
									</div>
								</div>
							</div>
							<div class="row mt-3 mb-3 admin-title">
								<div class="col-12">
									<h5 class="pl-3">
										Redes sociales
									</h5>
								</div>
							</div>
							<div class="card shadow-sm mb-3">
								<div class="card-body">
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">
											Celular Whatsapp
										</label>
										<div class="col-sm-9">
											<input type="tel"
												class="form-control"
												name="sn_whatsapp"
												value="{{ $udet->whatsapp }}"
												maxlength="15"
												placeholder="Ejemplo: 123 456 789" />
											<div class="invalid-feedback">
												Debes ingresar tu Nombre
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">
											Usuario Skype
										</label>
										<div class="col-sm-9">
											<input type="text"
												class="form-control"
												name="sn_skype"
												value="{{ $udet->skype }}"
												maxlength="50" />
											<div class="invalid-feedback">
												Debes ingresar tu Nombre
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">
											Link de Facebook
										</label>
										<div class="col-sm-9">
											<input type="text"
												class="form-control"
												name="sn_facebook"
												value="{{ $udet->facebook }}"
												maxlength="200" />
											<div class="invalid-feedback">
												Debes ingresar tu Nombre
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">
											Link de Instagram
										</label>
										<div class="col-sm-9">
											<input type="text"
												class="form-control"
												name="sn_instagram"
												value="{{ $udet->instagram }}"
												maxlength="200" />
											<div class="invalid-feedback">
												Debes ingresar tu Nombre
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">
											Link de Twitter
										</label>
										<div class="col-sm-9">
											<input type="text"
												class="form-control"
												name="sn_twitter"
												value="{{ $udet->twitter }}"
												maxlength="200" />
											<div class="invalid-feedback">
												Debes ingresar tu Nombre
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">
											Link de Youtube
										</label>
										<div class="col-sm-9">
											<input type="text"
												class="form-control"
												name="sn_youtube"
												value="{{ $udet->youtube }}"
												maxlength="200" />
											<div class="invalid-feedback">
												Debes ingresar tu Nombre
											</div>
										</div>
									</div>
									<div class="form-group row mb-0">
										<label class="col-sm-3 col-form-label">
											Link de Pinterest
										</label>
										<div class="col-sm-9">
											<input type="text"
												class="form-control"
												name="sn_pinterest"
												value="{{ $udet->pinterest }}"
												maxlength="200" />
											<div class="invalid-feedback">
												Debes ingresar tu Nombre
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group text-right mt-4">
						<button class="btn btn-success btn-lg"
							type="submit" disabled>
							Grabar cambios
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script>
let $form;

function enableUI() {
  $form.find('button').removeAttr('disabled');
}

function disableUI() {
  $form.find('button').attr('disabled', true);
}

function handleForm() {
  if ($form[0].checkValidity() === false) {
    $form.addClass('was-validated');
    return;
	}
	
  disableUI();
  $('.loader').removeClass('d-none');

	$form.find('.form-control').removeClass('is-invalid');
  $form.find('.alert').addClass('d-none');

  $.ajax({
    method: 'POST',
    url: $form.data('url'),
    data: $form.serialize(),
    dataType: 'json',
  })
  .done(function(resp) {
    if (resp.success) {
			$form.find('#alert-form')
			.removeClass('alert-danger')
			.addClass('alert-success')
			.removeClass('d-none')
      .text(resp.message);
    }
    else {
      $form.find('#alert-form')
			.removeClass('alert-success')
			.addClass('alert-danger')
			.removeClass('d-none')
      .text(resp.message);
    	enableUI();
    }
  })
  .fail(function(resp) {
		$form.find('#alert-form')
		.removeClass('alert-success')
		.addClass('alert-danger')
		.removeClass('d-none')
		.text('Hay algunos errores que debe subsanar');

    let errors = resp.responseJSON.errors;

    $.each(errors, function(key, message) {
      $form.find(".form-control[name='"+key+"']")
        .addClass('is-invalid')
        .next()
        .text(message);
    });

    $form.addClass('needs-validation');
		enableUI();
  })
  .always(function() {
		$('.loader').addClass('d-none');
		$('html, body').animate({scrollTop: '0px'}, 1000);
  });
}

function upload($file) {
	$('.loader').removeClass('d-none');
	
	var formData = new FormData();
	formData.append('attachment', $file[0].files[0]);
	formData.append('_token', $("meta[name='csrf-token']").attr('content'));

	$.ajax({
			url: $file.data('url'),
			data: formData,
			type: 'POST',
			enctype: 'multipart/form-data',
			cache: false,
			contentType: false,
			processData: false,
	})
	.done(function(resp) {
			location.reload();
	})
	.fail(function(resp) {
			let mssg = resp.responseJSON.message;
			if (mssg) {
					$('#alert-upload').removeClass('d-none').text(mssg);
			}
	})
	.always(function() {
			$('.loader').addClass('d-none');
			$file.val("");
	});
}

$(document).ready(function() {
	$('#avatar-upload').change(function() {
		upload($(this));
	});

  $form = $('#form-profile');
  enableUI();
  
  $form.submit(function(e) {
    e.preventDefault();
    e.stopPropagation();
    handleForm();
  });
});
</script>
@endpush