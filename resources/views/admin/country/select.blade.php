@extends('layouts.app')

@section('title', 'Tudaton.com')

@section('content')
<section class="mt-40px mb-40px">
	<div class="container">
		<div class="row justify-content-center">
      <div class="col-12 col-sm-8 col-md-6 col-lg-4">
        <div id="alert-form" class="alert alert-warning text-center" role="alert">
          Antes de continuar, por favor, indícanos tu País.
        </div>
        <div class="card">
          <div class="card-body">
            <h3 class="text-center mb-4">Selecciona tu Pais</h3>
            <form method="post" action="{{ route('cgl.country.select.save') }}">
              @csrf
              <div class="form-group">
                <select class="form-control"
                  name="country"
                  required>
                  @foreach($countries as $row)
                    <option value="{{ $row->id }}">
                      {{ $row->name }}
                    </option>
                  @endforeach
                </select>
                @if (session('number'))
                  <input type="hidden" name="number" value="1" />
                @else
                  <input type="hidden" name="number" value="0" />
                @endif
                <div class="invalid-feedback">
                  Selecciona tu País
                </div>
              </div>
              <div class="form-group">
                <button class="btn btn-lg btn-primary btn-block mt-5"
                  type="submit">
                  Seleccionar
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
		</div>
	</div>
</section>
<footer class="footer">
</footer>
@endsection
