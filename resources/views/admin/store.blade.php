@extends('layouts.app')

@section('title', 'Tudaton.com')

@section('content')
<section class="mt-40px mb-50px">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-4">
        <x-admin-sidebar selected="STOR" />
			</div>
			<div class="col-lg-9">
				<form id="form-profile"
					data-url="{{ route('cgl.store.edit') }}"
					novalidate>
					@csrf
					<div id="alert-form" class="alert mb-30px d-none" role="alert">
					</div>
					<div class="row mt-3 mb-40px admin-title">
						<div class="col-12">
							<h4 class="pl-3">
								<i class="fas fa-store"></i> &nbsp;
								Mi Tienda
							</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-4">
							<div id="alert-banner-upload"
								class="alert alert-danger d-none"
								role="alert">
								&nbsp;
							</div>
							<div class="card m-auto admin-store-banner">
								@php
								if (empty($store->image_banner)) {
									$imageUrl = url('img/avatar.jpg');
								}
								else {
									$imageUrl = Storage::url(sha1($user->id).'/'.$store->image_banner);
								}
								@endphp
								<label for="banner-upload"
									class="m-0">
									<img src="{{ $imageUrl }}"
										class="card-img-top img-thumbnail"
										alt="Foto banner de tienda" />
									<div class="card-img-overlay">
										<p class="card-text text-center">
											<i class="fas fa-camera"></i><br>
											Subir Banner
										</p>
									</div>
								</label>
								<input type="file" id="banner-upload"
									accept="image/x-png,image/jpeg"
									data-url="{{ route('cgl.store.banner') }}" hidden />
							</div>
							<hr/>
							<div id="alert-logo-upload"
								class="alert alert-danger d-none"
								role="alert">
								&nbsp;
							</div>
							<div class="card m-auto admin-avatar">
								@php
								if (empty($store->image_logo)) {
									$imageUrl = url('img/avatar.jpg');
								}
								else {
									$imageUrl = Storage::url(sha1($user->id).'/'.$store->image_logo);
								}
								@endphp
								<label for="logo-upload"
									class="m-0">
									<img src="{{ $imageUrl }}"
										class="card-img-top img-thumbnail"
										alt="Foto logotipo de tienda" />
									<div class="card-img-overlay">
										<p class="card-text text-center">
											<i class="fas fa-camera"></i><br>
											Subir Logo
										</p>
									</div>
								</label>
								<input type="file" id="logo-upload"
									accept="image/x-png,image/jpeg"
									data-url="{{ route('cgl.store.logo') }}" hidden />
							</div>
						</div>
						<div class="col-12 col-sm-8">
							<div class="card">
								<div class="card-body">
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">
											Nombre
										</label>
										<div class="col-sm-9">
											<input type="text" name="name"
												class="form-control"
												value="{{ $store->name }}"
												maxlength="40"
												required />
											<div class="invalid-feedback">
												Debes ingresar tu Nombre
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-3 col-form-label">
											Dirección
										</label>
										<div class="col-sm-9">
											<input type="text" name="address"
												class="form-control"
												value="{{ $store->address }}"
												maxlength="200"
												required />
											<div class="invalid-feedback">
												Debes ingresar tu Nombre
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group text-right mt-4">
						<button class="btn btn-success btn-lg"
							type="submit" disabled>
							Grabar cambios
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script>
let $form;

function enableUI() {
  $form.find('button').removeAttr('disabled');
}

function disableUI() {
  $form.find('button').attr('disabled', true);
}

function handleForm() {
  if ($form[0].checkValidity() === false) {
    $form.addClass('was-validated');
    return;
	}
	
  disableUI();
  $('.loader').removeClass('d-none');

	$form.find('.form-control').removeClass('is-invalid');
  $form.find('.alert').addClass('d-none');

  $.ajax({
    method: 'POST',
    url: $form.data('url'),
    data: $form.serialize(),
    dataType: 'json',
  })
  .done(function(resp) {
    if (resp.success) {
			$form.find('#alert-form')
			.removeClass('alert-danger')
			.addClass('alert-success')
			.removeClass('d-none')
      .text(resp.message);
    }
    else {
      $form.find('#alert-form')
			.removeClass('alert-success')
			.addClass('alert-danger')
			.removeClass('d-none')
      .text(resp.message);
    	enableUI();
    }
  })
  .fail(function(resp) {
		$form.find('#alert-form')
		.removeClass('alert-success')
		.addClass('alert-danger')
		.removeClass('d-none')
		.text('Hay algunos errores que debe subsanar');

    let errors = resp.responseJSON.errors;

    $.each(errors, function(key, message) {
      $form.find(".form-control[name='"+key+"']")
        .addClass('is-invalid')
        .next()
        .text(message);
    });

    $form.addClass('needs-validation');
		enableUI();
  })
  .always(function() {
		$('.loader').addClass('d-none');
		$('html, body').animate({scrollTop: '0px'}, 1000);
  });
}

function uploadBanner($file) {
	$('.loader').removeClass('d-none');
	
	var formData = new FormData();
	formData.append('attachment', $file[0].files[0]);
	formData.append('_token', $("meta[name='csrf-token']").attr('content'));

	$.ajax({
			url: $file.data('url'),
			data: formData,
			type: 'POST',
			enctype: 'multipart/form-data',
			cache: false,
			contentType: false,
			processData: false,
	})
	.done(function(resp) {
			location.reload();
	})
	.fail(function(resp) {
			let mssg = resp.responseJSON.message;
			if (mssg) {
					$('#alert-banner-upload')
					.removeClass('d-none')
					.text(mssg);
			}
	})
	.always(function() {
			$('.loader').addClass('d-none');
			$file.val("");
	});
}

function uploadLogo($file) {
	$('.loader').removeClass('d-none');
	
	var formData = new FormData();
	formData.append('attachment', $file[0].files[0]);
	formData.append('_token', $("meta[name='csrf-token']").attr('content'));

	$.ajax({
			url: $file.data('url'),
			data: formData,
			type: 'POST',
			enctype: 'multipart/form-data',
			cache: false,
			contentType: false,
			processData: false,
	})
	.done(function(resp) {
			location.reload();
	})
	.fail(function(resp) {
			let mssg = resp.responseJSON.message;
			if (mssg) {
					$('#alert-logo-upload')
					.removeClass('d-none')
					.text(mssg);
			}
	})
	.always(function() {
			$('.loader').addClass('d-none');
			$file.val("");
	});
}

$(document).ready(function() {
	$('#banner-upload').change(function() {
		uploadBanner($(this));
	});

	$('#logo-upload').change(function() {
		uploadLogo($(this));
	});

  $form = $('#form-profile');
  enableUI();
  
  $form.submit(function(e) {
    e.preventDefault();
    e.stopPropagation();
    handleForm();
  });
});
</script>
@endpush