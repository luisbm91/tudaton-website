@extends('layouts.app')

@section('title', 'Tudaton.com')

@section('content')
<section class="mt-40px mb-50px">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-4">
        <x-admin-sidebar selected="LIST" />
			</div>
			<div class="col-lg-9">
        @if ($user->type_id == 3)
        <div class="row mt-2 mb-50px">
          <div class="col-12 col-sm-6 col-md-3 mb-3">
            <div class="card rounded bg-primary shadow-sm admin-total">
              <div class="card-body">
                <h3>12</h3>
                En total
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-3 mb-3">
            <div class="card rounded bg-success shadow-sm admin-total">
              <div class="card-body">
                <h3>3</h3>
                Aprobados
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-3 mb-3">
            <div class="card rounded bg-danger shadow-sm admin-total">
              <div class="card-body">
                <h3>5</h3>
                Expirados
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-3 mb-3">
            <div class="card rounded bg-warning shadow-sm admin-total">
              <div class="card-body">
                <h3>2</h3>
                Pendientes
              </div>
            </div>
          </div>
        </div>
        @endif
        <div class="row mt-3 mb-40px admin-title">
          <div class="col">
            <h4 class="pl-3">
              <i class="fas fa-bullhorn"></i> &nbsp;
              Mis anuncios
            </h4>
          </div>
          <div class="col text-right">
            <a href="{{ route('cgl.listing.create') }}"
              class="btn btn-success btn-lg shadow-sm">
              <i class="fas fa-plus"></i> &nbsp;
              Subir Nuevo Anuncio
            </a>
          </div>
        </div>
        @foreach($listings as $row)
          <div class="card admin-listing mt-3 shadow-sm">
            <div class="card-body">
              <div class="media">
                <img class="align-self-center"
                  src="{{ $row['image'] }}"
                  alt="{{ $row['title'] }}" />
                <div class="media-body">
                  <div class="row">
                    <div class="col-12 col-md-5">
                      <div class="mb-3">
                        <span class="badge {{ $row['status_cl'] }}">
                          {{ $row['status'] }}
                        </span>
                      </div>
                      <h5>{{ $row['title'] }}</h5>
                    </div>
                    <div class="col-12 col-md-2">
                      <h6>Expira</h6>
                      <p>{{ $row['date_pub'] }}</p>
                    </div>
                    <div class="col-12 col-md-2">
                      <h6>Listing ID</h6>
                      <p>{{ str_pad($row['id'], 6, "0", STR_PAD_LEFT) }}</p>
                    </div>
                    <div class="col-12 col-md-3">
                      <div class="dropdown">
                        <button class="btn btn-primary btn-block dropdown-toggle"
                          type="button" id="dropdownMenuButton"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false">
                          Configuraciones
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item"
                            href="{{ route('cgl.listing.information', [ 'id' => $row['id'] ]) }}">
                            <i class="fas fa-pencil-alt"></i> &nbsp;
                            Editar
                          </a>
                          <!--
                          <a class="dropdown-item" href="#">
                            <i class="fas fa-eye"></i> &nbsp;
                            Vista Previa
                          </a>
                          -->
                          <a class="dropdown-item" href="#">
                            <i class="fas fa-trash-alt"></i> &nbsp;
                            Borrar
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach
			</div>
		</div>
	</div>
</section>
<footer class="footer">
</footer>
@endsection

@push('scripts')
<script>
function changeStatus($button) {
    $('.loader').removeClass('d-none');

    $.ajax({
        url: $button.data('url'),
        type: 'POST',
        data: {
            _token: $("meta[name='csrf-token']").attr('content'),
            id: $button.data('id')
        },
        dataType: 'json',
    })
    .always(function() {
        $('.loader').addClass('d-none');
        location.reload();
    });
}

$(document).ready(function() {
    $('.ls-action').click(function() {
        changeStatus($(this));
    });
});
</script>
@endpush