@extends('layouts.app')

@section('title', 'Tudaton.com')

@section('content')
<section class="mt-40px mb-50px">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-4">
        <x-admin-sidebar selected="LIST" />
			</div>
			<div class="col-lg-9 mb-4">
				<div id="alert-form" class="alert mb-30px d-none" role="alert">
				</div>
				<div class="row mt-3 mb-40px admin-title">
					<div class="col-12">
						<h4 class="pl-3">
							<i class="fas fa-bullhorn"></i> &nbsp;
							Datos del Anuncio
						</h4>
					</div>
				</div>
				@if ($errors->any())
					<div class="row mb-3">
						<div class="col-12">
								<div id="alert-form"
									class="alert alert-danger text-left" role="alert">
									Existen errores que debe subsanar
								</div>
						</div>
					</div>
				@endif
				@if (session('success'))
					<div class="row mb-3">
						<div class="col-12">
							<div id="alert-form"
								class="alert alert-success text-left" role="alert">
								{{ session('success') }}
							</div>
						</div>
					</div>
        @endif
				<div class="row">
					<div class="col-12 col-md-3 mb-5">
						<x-wizard :listingId="$listing->id" option-selected="DELI" />
					</div>
					<div class="col-12 col-md-9">
						<div class="row mb-3 admin-title">
							<div class="col-12">
								<h5 class="pl-3">
									<i class="fas fa-truck"></i> &nbsp;
									Delivery
								</h5>
							</div>
						</div>
						<div class="card shadow-sm">
							<div class="card-body">
								<div class="row mb-4">
									<div class="col-12">
										<div id="alert-form"
											class="alert alert-info text-left" role="alert">
											<small>
												<strong>Sección Opcional:</strong> En caso ofrezcas el servicio 
												de delivery para tu anuncio/producto, ingresa aqui los datos correspondientes.
											</small>
										</div>
									</div>
								</div>
								<form id="form-delivery"
									method="post"
									action="{{ route('cgl.listing.delivery.save', [ 'id' => $listing->id ]) }}">
									@csrf
									<div class="row mb-5">
										<div class="col-12 text-left">
											<div class="form-check">
												<input class="form-check-input" 
													type="checkbox" name="has_delivery"
													value="1" id="has-delivery"
													@if ($listing->has_delivery === 1)
														checked
													@endif />
												<label class="form-check-label" for="has-delivery">
													<strong>SI</strong> deseo agregar delivery
												</label><br />
												<small>Click en el check en caso quieras agregar delivery</small>
											</div>
										</div>
									</div>
									<div class="row mb-3" id="toggle-delivery">
										<div class="col-12 col-md-6 text-left">
											<div class="form-check">
												<input class="form-check-input" type="radio" 
													name="delivery_level_2" 
													id="delivery-level-2-yes" value="1"
													@if ($listing->delivery_all_country === 1)
														checked
													@endif />
												<label class="form-check-label" for="delivery-level-2-yes">
													A todo el país: <strong>PERU</strong>
												</label>
											</div>
										</div>
										<div class="col-12 col-md-6 text-right">
											<div class="form-check">
												<input class="form-check-input" type="radio" 
													name="delivery_level_2" 
													id="delivery-level-2-no" value="0"
													@if ($listing->delivery_all_country != 1)
														checked
													@endif />
												<label class="form-check-label" for="delivery-level-2-no">
													Solo algunas ciudades
												</label>
											</div>
										</div>
									</div>
								</form>
								<div id="toggle-places">
									<div class="row mb-3">
										<div class="col-12 text-right">
											<button type="button" class="btn btn-info" 
												title="Añadir Ciudades">
												+ Ciudades
											</button>
										</div>
									</div>
									<table class="table" rol="presentation">
										<tr>
											<td>
												<span title="Ciudad de Lima">
													Lima
												</span>
											</td>
											<td class="text-right">
												<button type="button" class="btn btn-light" 
													title="Añadir Provincias">
													<span class="text-info">
														<i class="fas fa-plus"></i>
													</span>
												</button>
												<button type="button" class="btn btn-light" 
													title="Eliminar Ciudad">
													<span class="text-danger">
														<i class="fas fa-times"></i>
													</span>
												</button>
											</td>
										</tr>
										<tr>
											<td>
												<span class="ml-5" title="Provincia de Lima">
													Lima
												</span>
											</td>
											<td class="text-right">
												<button type="button" class="btn btn-light" 
													title="Añadir Distritos">
													<span class="text-info">
														<i class="fas fa-plus"></i>
													</span>
												</button>
												<button type="button" class="btn btn-light" 
													title="Eliminar Provincia">
													<span class="text-danger">
														<i class="fas fa-times"></i>
													</span>
												</button>
											</td>
										</tr>
										<tr>
											<td>
												<div class="ml-5">
													<span class="ml-5" title="Distrito de San Juan de Lurigancho">
														San Juan de Lurigancho
													</span>
												</div>
											</td>
											<td class="text-right">
												<button type="button" class="btn btn-light" 
													title="Eliminar Distrito">
													<span class="text-danger">
														<i class="fas fa-times"></i>
													</span>
												</button>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group text-right mt-4">
					<a class="btn btn-info btn-lg"
						href="{{ route('cgl.listing.photos', [ 'listingId' => $listing->id ]) }}">
						Omitir
					</a> &nbsp;
					<button class="btn btn-success btn-lg"
						type="submit" id="btn-grabar"
						form="form-delivery" disabled>
						Grabar Cambios
					</button>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script>
function enableUI() {
  $('#btn-grabar').removeAttr('disabled');
}

function disableUI() {
  $('#btn-grabar').attr('disabled', true);
}

function enableDelivery() {
	$('#toggle-delivery').removeClass('d-none');
}

function disableDelivery() {
	$('#toggle-delivery').addClass('d-none');
}

function enablePlaces() {
	$('#toggle-places').removeClass('d-none');
}

function disablePlaces() {
	$('#toggle-places').addClass('d-none');
}

function showPlacesLevel3(isInit) {
	const selected = $('#place_level_2').val();
	const $dropdown = $('#place_level_3');

	if (selected == '')
		return;

	$.get($dropdown.data('url'), 
	{ id : selected },
	function(data) {
		$dropdown.html(data);

		if (isInit) {
			const value = $dropdown.data('value');
			$dropdown.val(value);
			showPlacesLevel4(true);
		}
		else {
			$('#place_level_4').val('');
		}
	});
}

function showPlacesLevel4(isInit) {
	const selected = $('#place_level_3').val();
	const $dropdown = $('#place_level_4');

	if (selected == '')
		return;

	$.get($dropdown.data('url'), 
	{ id : selected },
	function(data) {
		$dropdown.html(data);

		if (isInit) {
			const value = $dropdown.data('value');
			$dropdown.val(value);
		}
	});
}

function toogleDelivery() {
	if ($('#has-delivery').is(':checked'))
		enableDelivery();
	else
		disableDelivery();
}

$(document).ready(function() {
  const $form = $('#form-delivery');
  const $loader = $('.loader');

  enableUI();
	toogleDelivery();
	disablePlaces();
	
	$('#has-delivery').click(toogleDelivery);
	
	$("input[name='delivery_level_2']").click(function() {
    if ($('#delivery-level-2-yes').is(':checked'))
			disablePlaces();
    else
			enablePlaces();
	});
	
  $form.submit(function(e) {
		disableUI();
		$loader.removeClass('d-none');
  });
});
</script>
@endpush