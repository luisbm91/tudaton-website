@extends('layouts.app')

@section('title', 'Tudaton.com')

@section('content')
<section class="mt-40px mb-50px">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-4">
        <x-admin-sidebar selected="LIST" />
			</div>
			<div class="col-lg-9 mb-4">
				<div id="alert-form" class="alert mb-30px d-none" role="alert">
				</div>
				<div class="row mt-3 mb-40px admin-title">
					<div class="col-12">
						<h4 class="pl-3">
							<i class="fas fa-bullhorn"></i> &nbsp;
							Datos del Anuncio
						</h4>
					</div>
				</div>
				@if ($errors->any())
					<div class="row mb-3">
						<div class="col-12">
								<div id="alert-form"
									class="alert alert-danger text-left" role="alert">
									Existen errores que debe subsanar
								</div>
						</div>
					</div>
				@endif
				@if (session('success'))
					<div class="row mb-3">
						<div class="col-12">
							<div id="alert-form"
								class="alert alert-success text-left" role="alert">
								{{ session('success') }}
							</div>
						</div>
					</div>
        @endif
				<div class="row">
					<div class="col-12 col-md-3 mb-5">
						<x-wizard :listingId="$listingId" option-selected="LOCA" />
					</div>
					<div class="col-12 col-md-9">
						<form id="form-location"
							method="post"
							action="{{ route('cgl.listing.location.save', [ 'id' => $listingId ]) }}">
							@csrf
							<div class="row mb-3 admin-title">
								<div class="col-12">
									<h5 class="pl-3">
										<i class="fas fa-map-marked-alt"></i> &nbsp;
										Ubicación
									</h5>
								</div>
							</div>
							<div class="card shadow-sm">
								<div class="card-body">
									<div class="row mb-4">
										<div class="col-12">
											<div id="alert-form"
												class="alert alert-info text-left" role="alert">
												<small>
													<strong>Sección Opcional:</strong> Es caso tu anuncio/producto
													requiera de una dirección física, esta es la sección correcta.
												</small>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label for="address">
											Dirección
										</label>
										<input type="text"
											name="address" id="address"
											class="form-control @error('address') is-invalid @enderror"
											value="{{ old('address', optional($location)->address) }}"
											maxlength="60"
											required />
										@error('address')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
									<div class="form-group row">
										<div class="col-6">
											<label for="place_level_2">
												Ciudad
											</label>
											<select name="place_level_2"
												id="place_level_2"
												class="form-control @error('place_level_2') is-invalid @enderror"
												required>
												<option value="">--</option>
												@foreach($places as $row)
													<option value="{{ $row->id }}"
														@if ($row->id == old('place_level_2', optional($location)->place_level_2)))
															selected
														@endif
														>
														{{ $row->name }}
													</option>
												@endforeach
											</select>
											@error('price')
												<div class="invalid-feedback">
													{{ $message }}
												</div>
											@enderror
										</div>
										<div class="col-6">
											<label for="place_level_3">
												Provincia
											</label>
											<select name="place_level_3"
												id="place_level_3"
												class="form-control @error('place_level_3') is-invalid @enderror"
												data-url="{{ route('cgl.place.dropdown') }}"
												data-value="{{ optional($location)->place_level_3 }}"
												required>
												<option value="">--</option>
											</select>
											@error('place_level_3')
												<div class="invalid-feedback">
													{{ $message }}
												</div>
											@enderror
										</div>
									</div>
									<div class="form-group row">
										<div class="col-6">
											<label for="place_level_4">
												Distrito
											</label>
											<select name="place_level_4"
												id="place_level_4"
												class="form-control @error('place_level_4') is-invalid @enderror"
												data-url="{{ route('cgl.place.dropdown') }}"
												data-value="{{ optional($location)->place_level_4 }}"
												required>
												<option value="">--</option>
											</select>
											@error('place_level_4')
												<div class="invalid-feedback">
													{{ $message }}
												</div>
											@enderror
										</div>
										<div class="col-6">
											&nbsp;
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="form-group text-right mt-4">
					<a class="btn btn-info btn-lg"
						href="{{ route('cgl.listing.delivery', [ 'id' => $listingId ]) }}">
						Omitir
					</a> &nbsp;
					<button class="btn btn-success btn-lg"
						type="submit" id="btn-grabar"
						form="form-location" disabled>
						Grabar y continuar
					</button>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script>
function enableUI() {
  $('#btn-grabar').removeAttr('disabled');
}

function disableUI() {
  $('#btn-grabar').attr('disabled', true);
}

function showPlacesLevel3(isInit) {
	const selected = $('#place_level_2').val();
	const $dropdown = $('#place_level_3');

	if (selected == '')
		return;

	$.get($dropdown.data('url'), 
	{ id : selected },
	function(data) {
		$dropdown.html(data);

		if (isInit) {
			const value = $dropdown.data('value');
			$dropdown.val(value);
			showPlacesLevel4(true);
		}
		else {
			$('#place_level_4').val('');
		}
	});
}

function showPlacesLevel4(isInit) {
	const selected = $('#place_level_3').val();
	const $dropdown = $('#place_level_4');

	if (selected == '')
		return;

	$.get($dropdown.data('url'), 
	{ id : selected },
	function(data) {
		$dropdown.html(data);

		if (isInit) {
			const value = $dropdown.data('value');
			$dropdown.val(value);
		}
	});
}

$(document).ready(function() {
  const $form = $('#form-location');
  const $loader = $('.loader');

  enableUI();
	showPlacesLevel3(true);
	
	$('#place_level_2').change(function() {
		showPlacesLevel3(false);
	});
	$('#place_level_3').change(function() {
		showPlacesLevel4(false);
	});
  
  $form.submit(function(e) {
		disableUI();
		$loader.removeClass('d-none');
  });
});
</script>
@endpush