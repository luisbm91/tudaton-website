@extends('layouts.app')

@section('title', 'Tudaton.com')

@section('content')
<section class="mt-40px mb-50px">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-4">
        <x-admin-sidebar selected="LIST" />
			</div>
			<div class="col-lg-9 mb-4">
				<div id="alert-form" class="alert mb-30px d-none" role="alert">
				</div>
				<div class="row mt-3 mb-40px admin-title">
					<div class="col-12">
						<h4 class="pl-3">
							<i class="fas fa-bullhorn"></i> &nbsp;
							Datos del Anuncio
						</h4>
					</div>
				</div>
				@if ($errors->any())
					<div class="row mb-3">
						<div class="col-12">
								<div id="alert-form"
									class="alert alert-danger text-left" role="alert">
									Existen errores que debe subsanar
								</div>
						</div>
					</div>
				@endif
				@if (session('success'))
					<div class="row mb-3">
						<div class="col-12">
							<div id="alert-form"
								class="alert alert-success text-left" role="alert">
								{{ session('success') }}
							</div>
						</div>
					</div>
        @endif
				<div class="row">
					<div class="col-12 col-md-3 mb-5">
						<x-wizard :listingId="$listing->id" option-selected="INFO" />
					</div>
					<div class="col-12 col-md-9">
						<form id="form-listing"
							method="post"
							action="{{ route('cgl.listing.information.save', [ 'id' => $listing->id ]) }}">
							@csrf
							<div class="row mb-3 admin-title">
								<div class="col-12">
									<h5 class="pl-3">
										<i class="fas fa-info"></i> &nbsp;
										Información general
									</h5>
								</div>
							</div>
							<div class="card shadow-sm mb-50px">
								<div class="card-body">
									<div class="form-group">
										<label>
											Título
										</label>
										<input type="text" name="title"
											class="form-control @error('title') is-invalid @enderror"
											value="{{ old('title', $listing->title) }}"
											maxlength="60"
											required />
										@error('title')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
									<div class="form-group row">
										<div class="col-6">
											<label>
												Precio
											</label>
											<input type="number" name="price"
												class="form-control @error('price') is-invalid @enderror"
												min="0" step="0.01"
												value="{{ old('price', $listing->price) }}"
												required />
											@error('price')
												<div class="invalid-feedback">
													{{ $message }}
												</div>
											@enderror
										</div>
										<div class="col-6">
											<label>
												Moneda
											</label>
											<select name="currency_id"
												class="form-control @error('currency_id') is-invalid @enderror"
												required>
												@foreach($currencies as $row)
													<option value="{{ $row->id }}"
														@if ($row->id == old('currency_id', $listing->currency_id))
															selected
														@endif
														>
														{{ $row->name }}
													</option>
												@endforeach
											</select>
											@error('currency_id')
												<div class="invalid-feedback">
													{{ $message }}
												</div>
											@enderror
										</div>
									</div>
								</div>
							</div>
							<div class="row mb-3 admin-title">
								<div class="col-12">
									<h5 class="pl-3">
										Descripción del Anuncio
									</h5>
								</div>
							</div>
							<div class="card shadow-sm">
								<div class="card-body">
									<div class="form-group">
										<textarea name="description"
											class="form-control @error('description') is-invalid @enderror"
											rows="10"
											maxlength="1500"
											required>{{ old('description', optional($description)->description) }}</textarea>
										@error('description')
										<div class="invalid-feedback">
											{{ $message }}
										</div>
										@enderror
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="form-group text-right mt-4">
					<button class="btn btn-success btn-lg"
						type="submit" id="btn-grabar"
						form="form-listing" disabled>
						Grabar y continuar
					</button>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script>
function enableUI() {
  $('#btn-grabar').removeAttr('disabled');
}

function disableUI() {
  $('#btn-grabar').attr('disabled', true);
}

$(document).ready(function() {
  let $form = $('#form-listing');
  let $loader = $('.loader');

  enableUI();
  
  $form.submit(function(e) {
		disableUI();
		$loader.removeClass('d-none');
  });
});
</script>
@endpush