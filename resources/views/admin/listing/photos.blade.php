@extends('layouts.app')

@section('title', 'Tudaton.com')

@section('content')
<section class="mt-40px mb-50px">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-4">
        <x-admin-sidebar selected="LIST" />
			</div>
			<div class="col-lg-9 mb-4">
				<div id="alert-form" class="alert mb-30px d-none" role="alert">
				</div>
				<div class="row mt-3 mb-40px admin-title">
					<div class="col-12">
						<h4 class="pl-3">
							<i class="fas fa-bullhorn"></i> &nbsp;
							Datos del Anuncio
						</h4>
					</div>
				</div>
				@if ($errors->any())
					<div class="row mb-3">
						<div class="col-12">
								<div id="alert-form"
									class="alert alert-danger text-left" role="alert">
                  Hubo un error al intentar eliminar su fotografia.
								</div>
						</div>
					</div>
				@endif
        <div class="row">
          <div class="col-12 col-md-3 mb-5">
						<x-wizard :listingId="$listingId" option-selected="PHOT" />
          </div>
          <div class="col-12 col-md-9">
            <div class="row mb-3 admin-title">
              <div class="col-12">
                <h5 class="pl-3">
                  <i class="fas fa-camera-retro"></i> &nbsp;
                  Fotografias
                </h5>
              </div>
            </div>
            <div class="card shadow-sm">
              <div class="card-body">
                <div class="row">
                  <div class="col-12">
                    <label class="admuploadbox m-auto"
                      for="photo-upload">
                      <i class="fas fa-file-upload"></i>
                      <span>Adjuntar</span>
                    </label>
                    <input type="file" id="photo-upload"
                      accept="image/x-png,image/jpeg"
                      data-url="{{ route('cgl.listing.photos.upload', [ 'listingId' => $listingId ]) }}" hidden
                      disabled required />
                  </div>
                </div>
                <div class="row mt-50px">
                  @foreach($photos as $row)
                    <div class="col-md-4 col-6 mb-3">
                      <div class="card">
                        <img src="{{ Storage::url(sha1($userId).'/'.$row->name) }}"
                          class="card-img-top"
                          alt="Fotografía de producto" />
                        <div class="card-body text-center">
                          <button class="btn btn-danger btn-sm rounded btn-photo-delete"
                            title="Eliminar Fotografía"
                            data-photo="{{ $row->sequence_number }}">
                            <i class="fas fa-trash-alt"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
			</div>
		</div>
	</div>
</section>
<form id="photo-delete-form" method="POST" hidden
  action="{{ route('cgl.listing.photos.delete', [ 'listingId' => $listingId ]) }}">
  @csrf
  <input type="hidden" name="_method" value="DELETE" />
  <input type="hidden" name="photo" id="photo-delete-field" />
</form>
@endsection

@push('scripts')
<script>
function enableUI() {
  $('#photo-upload').removeAttr('disabled');
}

function upload($file) {
  $('.loader').removeClass('d-none');
  
  var formData = new FormData();
  formData.append('attachment', $file[0].files[0]);
  formData.append('_token', $("meta[name='csrf-token']").attr('content'));

  $.ajax({
    url: $file.data('url'),
    data: formData,
    type: 'POST',
    enctype: 'multipart/form-data',
    cache: false,
    contentType: false,
    processData: false,
  })
  .done(function(resp) {
    location.reload();
  })
  .fail(function(resp) {
    let mssg = resp.responseJSON.message;
    if (mssg) {
      $('#alert-form').removeClass('d-none').text(mssg);
    }
  })
  .always(function(resp) {
    $('.loader').addClass('d-none');
    $file.val("");
  });
}

$(document).ready(function() {
  enableUI();

  $('#photo-upload').change(function() {
    upload($(this));
  });

  $('.btn-photo-delete').click(function() {
		$('.loader').removeClass('d-none');
    const photo = $(this).data('photo');
    $('#photo-delete-field').val(photo);
    $('#photo-delete-form').submit();
  });
});
</script>
@endpush