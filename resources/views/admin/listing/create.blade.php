@extends('layouts.app')

@section('title', 'Tudaton.com')

@section('content')
<section class="mt-40px mb-50px">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-4">
        <x-admin-sidebar selected="LIST" />
			</div>
			<div class="col-lg-9 mb-4">
				<div id="alert-form" class="alert mb-30px d-none" role="alert">
				</div>
				<div class="row mt-3 mb-40px admin-title">
					<div class="col-12">
						<h4 class="pl-3">
							<i class="fas fa-plus"></i> &nbsp;
							Crear Anuncio
						</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						@if ($errors->any())
							<div id="alert-form"
								class="alert alert-danger text-left" role="alert">
								Existen errores que debe subsanar
							</div>
						@endif
						<form id="form-listing"
							method="post"
							action="{{ route('cgl.listing.create.save') }}">
							@csrf
							<div class="row mb-3 admin-title">
								<div class="col-12">
									<h5 class="pl-3">
										Selecciona una categoría
									</h5>
								</div>
							</div>
							<div class="card shadow-sm mb-50px">
								<div class="card-body">
									<div class="form-group">
										<label>
											Categoría
										</label>
										<select name="category_id"
											class="form-control @error('category_id') is-invalid @enderror"
											required>
											<option value="">--</option>
											@foreach($categories as $row)
												<option value="{{ $row->id }}">
													{{ $row->name }}
												</option>
											@endforeach
										</select>
										@error('category_id')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="form-group text-right mt-4">
					<button class="btn btn-success btn-lg"
						type="submit" id="btn-grabar"
						form="form-listing" disabled>
						Continuar
					</button>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script>
function enableUI() {
  $('#btn-grabar').removeAttr('disabled');
}

function disableUI() {
  $('#btn-grabar').attr('disabled', true);
}

$(document).ready(function() {
  let $form = $('#form-listing');
  let $loader = $('.loader');

  enableUI();
  
  $form.submit(function(e) {
		disableUI();
		$loader.removeClass('d-none');
  });
});
</script>
@endpush