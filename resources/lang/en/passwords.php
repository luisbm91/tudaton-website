<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Tu Contraseña ha sido reseteada!',
    'sent' => 'Te hemos enviado un Link a tu correo para que puedas resetear tu Contraseña!',
    'throttled' => 'Por favor, espere un momento y vuelva a intentarlo.',
    'token' => 'El enlace para resetear tu contraseña es inválido.',
    'user' => "No encontramos ningun usuario con el email ingresado.",

];
