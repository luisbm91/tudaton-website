<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomePageController@show')->name('home');
Route::get('anuncios', 'ListingsPageController@show')->name('listings');
Route::get('nosotros', 'AboutusPageController@show')->name('aboutus');
Route::get('tiendas', 'StoresPageController@show')->name('stores');

Route::get('anuncio/{id}', 'ListingDetailController@show')
    ->where('id', '[0-9]+')->name('ad.detail');
Route::post('get-places', 'PlaceController@handle')->name('get.places');
Route::post('get-categories', 'CategoryController@handle')->name('get.categories');

Route::middleware('guest')->group(function() {
	Route::get('login', 'Auth\LoginController@show')->name('login');
    Route::post('login', 'Auth\LoginController@handle');
    Route::get('register', 'Auth\RegisterController@show')->name('register');
    Route::post('register', 'Auth\RegisterController@handle');
    Route::get('password/reset', 'Auth\ForgotPasswordController@show')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@handle');
    Route::get('login/facebook', 'Auth\SocialLoginFacebookController@redirectToProvider');
    Route::get('login/facebook/callback', 'Auth\SocialLoginFacebookController@handleProviderCallback');
});

Route::post('password/email', 'Auth\ForgotPasswordController@handle')
	->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@show')
    ->name('password.token');
    
Route::middleware('auth')->group(function() {

    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    Route::namespace('Admin')->group(function() {

        // places dropdown
        Route::get('cgl/place-dropdown', 'PlaceDropdownController@handle')
            ->name('cgl.place.dropdown');

        // country
        Route::get('cgl/country-select', 'CountrySelectController@show')
            ->name('cgl.country.select');
        Route::post('cgl/country-select/save', 'CountrySelectController@handle')
            ->name('cgl.country.select.save');

        // user admin
        Route::get('cgl/dashboard', 'DashboardController@show')
            ->name('cgl.dashboard');

        Route::get('cgl/profile', 'ProfileController@show')
            ->name('cgl.profile');

        Route::post('cgl/profile/edit', 'ProfileController@handle')
            ->name('cgl.profile.edit');

        Route::post('/cgl/avatar', 'AvatarController@save')
            ->name('cgl.avatar');

        // listing create
        Route::get('/cgl/listing/create', 'ListingCreateController@show')
            ->name('cgl.listing.create');
        Route::post('/cgl/listing/create', 'ListingCreateController@handle')
            ->name('cgl.listing.create.save');
            
        // listing information
        Route::get('/cgl/listing/{id}/information', 'ListingEditController@show')
            ->where('id', '[0-9]+')->name('cgl.listing.information');
        Route::post('/cgl/listing/{id}/information', 'ListingEditController@handle')
            ->where('id', '[0-9]+')->name('cgl.listing.information.save');
            
        // listing location
        Route::get('/cgl/listing/{id}/location', 'ListingLocationController@show')
            ->where('id', '[0-9]+')->name('cgl.listing.location');
        Route::post('/cgl/listing/{id}/location', 'ListingLocationController@handle')
            ->where('id', '[0-9]+')->name('cgl.listing.location.save');
            
        // listing delivery
        Route::get('/cgl/listing/{id}/delivery', 'ListingDeliveryController@show')
            ->where('id', '[0-9]+')->name('cgl.listing.delivery');

        Route::post('/cgl/listing/{id}/delivery', 'ListingDeliveryController@handle')
            ->where('id', '[0-9]+')->name('cgl.listing.delivery.save');

        // listing photos
        Route::get('cgl/listing/{listingId}/photos', 'PhotoController@show')
            ->where('listingId', '[0-9]+')->name('cgl.listing.photos');

        Route::post('cgl/listing/{listingId}/photo-upload', 'PhotoController@upload')
            ->where('listingId', '[0-9]+')->name('cgl.listing.photos.upload');

        Route::delete('cgl/listing/{listingId}/photo-delete', 'PhotoController@delete')
            ->where('listingId', '[0-9]+')->name('cgl.listing.photos.delete');

        // listing enable disable
        Route::post('/cgl/listing/disable', 'ListingStatusController@disable')
            ->name('cgl.listing.disable');
            
        Route::post('cgl/listing/enable', 'ListingStatusController@enable')
            ->name('cgl.listing.enable');
            
        // store
        Route::get('cgl/store', 'StoreController@show')
            ->name('cgl.store');

        Route::post('cgl/store/edit', 'StoreController@handle')
            ->name('cgl.store.edit');

        Route::post('/cgl/store/logo', 'StoreUploadController@handleLogo')
            ->name('cgl.store.logo');

        Route::post('/cgl/store/banner', 'StoreUploadController@handleBanner')
            ->name('cgl.store.banner');

        Route::get('cgl/plan/purchase', 'PlanController@show')
            ->name('cgl.plan.purchase');

        Route::post('cgl/store/deal', 'StoreController@deal')
            ->name('cgl.store.deal');

    });

});