<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'public.currencies';

    public $timestamps = false;
    
}
