<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\StatusUser;
use App\TypeUser;

class AdminSidebar extends Component
{
    public $userName;
    public $avatar;
    public $statusName;
    public $statusClass;
    public $typeId;
    public $typeName;
    public $selected;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($selected)
    {
        $user = Auth::user();
        
        $this->userName = $user->name;
        $this->typeId = $user->type_id;
        $this->selected = $selected;

        if (empty($user->image)) {
            $this->avatar = url('img/avatar.jpg');
        } else {
            $this->avatar = Storage::url(sha1($user->id).'/'.$user->image);
        }
        
        $status = StatusUser::where('id', $user->status_id)
                ->select('name', 'css_class')->first();

        $this->statusName = $status->name;
        $this->statusClass = $status->css_class;

        $type = TypeUser::where('id', $user->type_id)
                ->select('name')->first();

        $this->typeName = $type->name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.admin-sidebar');
    }
}
