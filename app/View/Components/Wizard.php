<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Photo;
use App\ListingLocation;

class Wizard extends Component
{
    public $options;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($listingId, $optionSelected)
    {
        $countLocation = ListingLocation::where('listing_id', $listingId)->count();
        $countPhotos = Photo::where('listing_id', $listingId)->count();

        $this->options = [
            [
                'title' => 'Información',
                'route' => route('cgl.listing.information', [ 'id' => $listingId ]),
                'hasCheck' => true,
                'selected' => ($optionSelected === 'INFO'),
            ],
            [
                'title' => 'Ubicación',
                'route' => route('cgl.listing.location', [ 'id' => $listingId ]),
                'hasCheck' => ($countLocation > 0),
                'selected' => ($optionSelected === 'LOCA'),
            ],
            [
                'title' => 'Delivery',
                'route' => route('cgl.listing.delivery', [ 'id' => $listingId ]),
                'hasCheck' => false,
                'selected' => ($optionSelected === 'DELI'),
            ],
            [
                'title' => 'Fotografías',
                'route' => route('cgl.listing.photos', [ 'listingId' => $listingId ]),
                'hasCheck' => ($countPhotos > 0),
                'selected' => ($optionSelected === 'PHOT'),
            ],
        ];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.wizard');
    }
}
