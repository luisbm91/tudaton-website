<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingDelivery extends Model
{

    protected $table = 'public.listing_deliveries';

    protected $primaryKey = 'listing_id';
    
    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'listing_id', 'sequence_number', 'place_id', 'level'
    ];

}
