<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeUser extends Model
{
    protected $table = 'public.types_users';
    
    public $incrementing = false;

    public $timestamps = false;
}
