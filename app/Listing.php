<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    
    protected $table = 'public.listings';

    protected $fillable = [
        'title', 'currency_id', 'price', 'category_id', 'user_id', 'status_id', 'description'
    ];

}
