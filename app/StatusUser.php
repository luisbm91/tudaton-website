<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusUser extends Model
{
    protected $table = 'public.statuses_users';
    
    public $incrementing = false;

    public $timestamps = false;
}
