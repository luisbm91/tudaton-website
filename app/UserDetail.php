<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $table = 'public.users_details';

    protected $primaryKey = 'user_id';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'user_id', 'whatsapp', 'skype', 'facebook', 'instagram',
        'twitter', 'youtube', 'pinterest', 'description'
    ];
}
