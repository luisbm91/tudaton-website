<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingLocation extends Model
{

    protected $table = 'public.listing_locations';

    protected $primaryKey = 'listing_id';
    
    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'listing_id', 'address', 'place_level_2', 'place_level_3', 'place_level_4'
    ];

}
