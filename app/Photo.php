<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'public.photos';

    protected $primaryKey = ['listing_id', 'sequence_number'];

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'listing_id', 'sequence_number', 'name'
    ];
}
