<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'public.stores';

    protected $primaryKey = 'user_id';
}
