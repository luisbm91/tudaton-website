<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $table = 'public.actions';
    
    public $timestamps = false;
    
}
