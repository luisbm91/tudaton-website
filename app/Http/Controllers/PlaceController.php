<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Place;

class PlaceController extends Controller
{
	public function handle(Request $request)
	{
        $parentId = intval($name = $request->input('id'));
        
        $places = Place::where('parent_id', $parentId)
                    ->select('id', 'name')
                    ->orderBy('name', 'asc')
                    ->get();
                    
        $result = array();
        foreach ($places as $key => $val)
        {
            $count = Place::where('parent_id', $val->id)
                    ->count();

            $result[$key] = [
                'id'        => $val->id,
                'name'      => $val->name,
                'hasc'      => ($count == 0 ? 0 : 1),
            ];
        }

        return $result;
    }

}
