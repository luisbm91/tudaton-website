<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Store;

class StoresPageController extends Controller
{
    public function show()
    {
        $isPageStores = TRUE;
        $stores = Store::all();

        return view('pages.stores', compact('isPageStores', 'stores'));
    }
}
