<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Action;
use App\Listing;
use App\Currency;
use App\Category;
use App\Photo;

class ListingsPageController extends Controller
{
	public function show(Request $request)
	{
        $isPageListings = TRUE;

        $term = $request->input('q');

        $actions = Action::all();

        $listings = Listing::where('status_id', 2)
                    ->where('title', 'like', '%'.$term.'%')
                    ->select('id', 'title', 'price', 'currency_id',
                    'category_id', 'published_at', 'user_id')
                    ->orderBy('id', 'desc')
                    ->limit(15)
                    ->get();
                    
        $arrListings = array();
        foreach ($listings as $key => $val)
        {
            $currn = Currency::where('id', $val->currency_id)
                    ->select('name')->first();

            $categ = Category::where('id', $val->category_id)
                    ->select('name')->first();

            $photo = Photo::where('listing_id', $val->id)
                    ->select('name')->limit(1)->first();

            if (isset($photo->name)) {
                $image = Storage::url(sha1($val->user_id).'/'.$photo->name);
            }
            else {
                $image = url('img/no-image.jpg');
            }

            $date = date('d/m/Y', strtotime($val->published_at));
            
            $arrListings[$key] = [
                'id'        => $val->id,
                'title'     => strtoupper($val->title),
                'price'     => number_format(round($val->price, 2)),
                'image'     => $image,
                'currency'  => $currn->name,
                'category'  => $categ->name,
                'date'      => $date,
            ];
        }

        return view('pages.listings',
            compact('isPageListings', 'actions', 'arrListings', 'request')
        );
	}
}
