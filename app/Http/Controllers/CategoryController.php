<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
	public function handle()
	{
        $result = Category::select('id', 'name')
                    ->orderBy('name', 'asc')
                    ->get();
                    
        return $result;
    }

}
