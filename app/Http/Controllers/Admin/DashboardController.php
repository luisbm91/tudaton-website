<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Listing;
use App\Photo;
use App\StatusListing;

class DashboardController extends Controller
{
	public function show()
	{
        $user = Auth::user();
        
        $rows = Listing::where('user_id', $user->id)
                ->select('id', 'title', 'status_id', 'published_at')
                ->orderBy('id', 'desc')
                ->get();

        $listings = array();

        foreach ($rows as $key => $val)
        {
            $status = StatusListing::where('id', $val->status_id)
                    ->select('name')->first();

            $photo = Photo::where('listing_id', $val->id)
                    ->select('name')->first();
            
            if (isset($photo->name)) {
                $imageUrl = Storage::url(sha1($user->id).'/'.$photo->name);
            }
            else {
                $imageUrl = url('img/no-image.jpg');
            }

            switch ($val->status_id)
            {
                case 2:
                    $badgeClass = 'badge-success';
                    break;

                case 3:
                    $badgeClass = 'badge-danger';
                    break;

                case 4:
                    $badgeClass = 'badge-warning';
                    break;
                
                default:
                    $badgeClass = 'badge-secondary';
                    break;
            }

            $listings[$key] = [
                'id'        => $val->id,
                'title'     => $val->title,
                'image'     => $imageUrl,
                'status'    => $status->name,
                'status_id' => $val->status_id,
                'status_cl' => $badgeClass,
                'date_pub'  => date("d/m/Y", strtotime($val->published_at)),
            ];
        }

        return view('admin.dashboard',
            compact('listings', 'user')
        );
	}
}
