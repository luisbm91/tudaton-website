<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Listing;

class ListingStatusController extends Controller
{
	public function enable(Request $request)
	{
    	$params = $request->validate([
    		'id' => 'required|numeric',
        ]);

        $listingId = intval($params['id']);
        
        $result = Listing::where([
                    'id'        => $listingId,
                    'user_id'   => Auth::id()
                ])->select('id')->first();
        
        if (!$result)
            return;

        Listing::where('id', $listingId)
            ->update(['status_id' => 2]);
    }

	public function disable(Request $request)
	{
    	$params = $request->validate([
    		'id' => 'required|numeric',
        ]);

        $listingId = intval($params['id']);
        
        $result = Listing::where([
                    'id'        => $listingId,
                    'user_id'   => Auth::id()
                ])->select('id')->first();

        if (!$result)
            return;
        
        Listing::where('id', $listingId)
            ->update(['status_id' => 3]);
    }
}
