<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\User;
use Image;

class AvatarController extends Controller
{
    public function save(Request $request)
    {
    	$params = $request->validate([
    		'attachment' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $user = Auth::user();
        
        $userId     = $user->id;
        $userFolder = sha1($userId);
        $publicPath = $userFolder.DIRECTORY_SEPARATOR;
        $exists     = Storage::disk('public')->exists($publicPath.$user->image);

        if ($exists) {
            Storage::disk('public')->delete($publicPath.$user->image);
        }
        else
        {
            $filesIn = Storage::disk('public')->files($publicPath);

            if (count($filesIn) == 0) {
                Storage::disk('public')->makeDirectory($publicPath);
            }
        }

        $image = $request->file('attachment');
        $avatarName = sha1(rand()).time().'.'.$image->getClientOriginalExtension();

        // Storage::disk('public')->put($publicPath.$avatarName, file_get_contents($image->getRealPath()), 'public');

        $imgResize = Image::make($image->getRealPath());
        $imgResize->resize(350, 350);
        $imgResize->save(
            storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$publicPath.$avatarName)
        );
        
        User::where('id', $userId)->update(['image' => $avatarName]);
    }
}
