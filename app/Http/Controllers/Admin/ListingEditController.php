<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Currency;
use App\Category;
use App\Listing;
use App\ListingDescription;

class ListingEditController extends Controller
{
    private $redirectTo;
    
    public function __construct() {
        $this->redirectTo = 'cgl.dashboard';
    }

	public function show($id)
	{
        if (Str::of($id)->trim()->isEmpty())
            return redirect()->route($this->redirectTo);
            
        $listing = Listing::where(['id' => $id, 'user_id' => Auth::id()])
            ->first();
        
        if (! $listing)
            return redirect()->route($this->redirectTo);
            
        $description = ListingDescription::find($id);
        $currencies = Currency::get();
        
        return view(
            'admin.listing.information',
            compact([
                'listing',
                'currencies',
                'description'
            ])
        );
    }
    
    public function handle($id, Request $request)
    {
        if (Str::of($id)->trim()->isEmpty())
            return redirect()->route($this->redirectTo);
            
        $listing = Listing::select('id')
            ->where(['id' => $id, 'user_id' => Auth::id()])
            ->first();
        
        if (! $listing)
            return redirect()->route($this->redirectTo);
            
        $data = $request->validate([
            'title'         => 'required|string|max:60',
            'price'         => 'required|numeric',
            'currency_id'   => 'required|numeric',
            'description'   => 'required|string|max:1500',
        ]);

        Listing::where('id', $listing->id)
            ->update(Arr::except($data, ['description']));

        $description = ListingDescription::find($listing->id);

        if (! $description)
        {
            $description = new ListingDescription;
            $description->listing_id = $listing->id;
            $description->description = $data['description'];
            $description->save();
        }
        else
        {
            $description->description = $data['description'];
            $description->save();
        }

        return redirect()->route(
            'cgl.listing.location', [ 'id' => $listing->id]
        );
    }

}
