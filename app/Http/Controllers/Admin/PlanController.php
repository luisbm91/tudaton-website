<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\TypeUser;
use App\StatusUser;

class PlanController extends Controller
{
	public function show()
	{
        $user = Auth::user();
        $utyp = TypeUser::find($user->type_id);
        $usta = StatusUser::find($user->status_id);
        
        return view(
            'admin.plan',
            compact([
                'user',
                'utyp',
                'usta'
            ])
        );
    }
}
