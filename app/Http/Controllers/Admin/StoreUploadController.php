<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Store;
use Image;

class StoreUploadController extends Controller
{

    public function handleBanner(Request $request)
    {
    	$params = $request->validate([
    		'attachment' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $user = Auth::user();
        $store = Store::find($user->id);
        
        $userFolder = sha1($user->id);
        $publicPath = $userFolder.DIRECTORY_SEPARATOR;
        $exists     = Storage::disk('public')->exists($publicPath.$store->image_banner);

        if ($exists) {
            Storage::disk('public')->delete($publicPath.$store->image_banner);
        }
        else
        {
            $filesIn = Storage::disk('public')->files($publicPath);

            if (count($filesIn) == 0) {
                Storage::disk('public')->makeDirectory($publicPath);
            }
        }

        $image = $request->file('attachment');
        $imageName = sha1(rand()).time().'.'.$image->getClientOriginalExtension();
        
        $imgResize = Image::make($image->getRealPath());
        $imgResize->resize(900, 300);
        $imgResize->save(
            storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$publicPath.$imageName)
        );
        
        Store::where('user_id', $user->id)
            ->update(['image_banner' => $imageName]);
    }

    public function handleLogo(Request $request)
    {
    	$params = $request->validate([
    		'attachment' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $user = Auth::user();
        $store = Store::find($user->id);
        
        $userFolder = sha1($user->id);
        $publicPath = $userFolder.DIRECTORY_SEPARATOR;
        $exists     = Storage::disk('public')->exists($publicPath.$store->image_logo);

        if ($exists) {
            Storage::disk('public')->delete($publicPath.$store->image_logo);
        }
        else
        {
            $filesIn = Storage::disk('public')->files($publicPath);

            if (count($filesIn) == 0) {
                Storage::disk('public')->makeDirectory($publicPath);
            }
        }

        $image = $request->file('attachment');
        $imageName = sha1(rand()).time().'.'.$image->getClientOriginalExtension();
        
        $imgResize = Image::make($image->getRealPath());
        $imgResize->resize(350, 350);
        $imgResize->save(
            storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$publicPath.$imageName)
        );
        
        Store::where('user_id', $user->id)
            ->update(['image_logo' => $imageName]);
    }

}
