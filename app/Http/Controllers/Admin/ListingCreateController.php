<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Currency;
use App\Category;
use App\Listing;

class ListingCreateController extends Controller
{
	public function show()
	{
        $user = Auth::user();

        if (!$user->country_id)
            return redirect()->
            route('cgl.country.select')
            ->with('number', 1);
            
        $qtyListings = Listing::where('user_id', $user->id)->count();

        if ($qtyListings > 25)
            return redirect()->route('cgl.plan.purchase');

        $categories = Category::get();
        
        return view(
            'admin.listing.create',
            compact([
                'categories',
                'user',
            ])
        );
    }
    
    public function handle(Request $request)
    {
        $data = $request->validate([
            'category_id' => 'required|numeric',
        ]);

        $user = Auth::user();

        $listing = new Listing;
        $listing->category_id   = intval($data['category_id']);
        $listing->user_id       = $user->id;
        $listing->status_id     = 1;
        $listing->country_id    = $user->country_id;
        $listing->save();
        
        return redirect()->route(
            'cgl.listing.information', [ 'id' => $listing->id ]
        );
    }

}
