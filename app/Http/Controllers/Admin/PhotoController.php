<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Listing;
use App\Photo;
use Image;

class PhotoController extends Controller
{
    private $redirectTo;
    
    public function __construct() {
        $this->redirectTo = 'cgl.dashboard';
    }

	public function show($listingId)
	{
        if (Str::of($listingId)->trim()->isEmpty())
            return redirect()->route($this->redirectTo);
            
        $userId = Auth::id();
            
        $listing = Listing::select('id')
            ->where(['id' => $listingId, 'user_id' => $userId])
            ->first();
        
        if (! $listing)
            return redirect()->route($this->redirectTo);
            
        $photos = Photo::where('listing_id', $listingId)->get();
        
        return view(
            'admin.listing.photos',
            compact([
                'photos',
                'listingId',
                'userId',
            ])
        );
    }
    
    public function upload($listingId, Request $request)
    {
        if (Str::of($listingId)->trim()->isEmpty())
            return;
            
        $userId = Auth::id();
            
        $listing = Listing::select('status_id', 'id')
            ->where(['id' => $listingId, 'user_id' => $userId])
            ->first();
            
        if (! $listing)
            return;

    	$data = $request->validate([
    		'attachment' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        
        $publicPath = sha1($userId).'/';
        $filesIn    = Storage::files($publicPath);

        if (count($filesIn) == 0) {
            Storage::makeDirectory($publicPath);
        }
        
        $image = $request->file('attachment');
        $attachmentName = sha1(rand()).time().'.'.$image->getClientOriginalExtension();

        $imgResize = Image::make($image->getRealPath());
        $imgResize->resize(700, 600);
        $imgResize->save(
            storage_path('app/public/'.$publicPath.$attachmentName)
        );

        $maxSequence = Photo::where('listing_id', $listingId)->max('sequence_number');
        $nextSequence = intval($maxSequence) + 1;

        Photo::create([
            'listing_id'        => $listingId,
            'sequence_number'   => $nextSequence,
            'name'              => $attachmentName,
        ]);

        if ($listing->status_id == 1) {
            Listing::where('id', $listingId)->update(['status_id' => 2]);
        }
    }
    
    public function delete($listingId, Request $request)
    {
        if (Str::of($listingId)->trim()->isEmpty())
            return redirect()->route($this->redirectTo);

        $userId = Auth::id();
            
        $listing = Listing::select('id')
            ->where(['id' => $listingId, 'user_id' => $userId])
            ->first();
            
        if (! $listing)
            return redirect()->route($this->redirectTo);
            
        $data = $request->validate([
    		'photo' => 'required|numeric'
        ]);

        $photo = Photo::select('name')
            ->where(['listing_id' => $listingId, 'sequence_number' => $data['photo']])
            ->first();
            
        Storage::delete(sha1($userId).'/'.$photo->name);
        
        Photo::where(['listing_id' => $listingId, 'sequence_number' => $data['photo']])
            ->delete();
            
        return back();
    }

}
