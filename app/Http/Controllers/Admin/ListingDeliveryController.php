<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Listing;
use App\ListingDelivery;
use App\Place;

class ListingDeliveryController extends Controller
{
    private $redirectTo;
    
    public function __construct() {
        $this->redirectTo = 'cgl.dashboard';
    }

	public function show($id)
	{
        if (empty($id))
            return redirect()->route($this->redirectTo);
            
        $listing = Listing::where(['id' => $id, 'user_id' => Auth::id()])->first();
        
        if (! $listing)
            return redirect()->route($this->redirectTo);
            
        $delivery = ListingDelivery::where('listing_id', $id)->first();
        $places = Place::where('parent_id', 10)->orderBy('name', 'asc')->get();
        
        return view(
            'admin.listing.delivery',
            compact([
                'listing',
                'delivery',
                'places',
            ])
        );
    }

	public function handle($id, Request $request)
	{
        if (empty($id))
            return redirect()->route($this->redirectTo);
            
        $listing = Listing::where(['id' => $id, 'user_id' => Auth::id()])->first();

        if (! $listing)
            return redirect()->route($this->redirectTo);

        $hasDelivery = (boolean) $request->input('has_delivery');

        if (! $hasDelivery) {
            ListingDelivery::where('listing_id', $listing->id)->delete();
        }
        
        $allCountry = (boolean) $request->input('delivery_level_2');

        $listing->has_delivery = $hasDelivery;
        $listing->delivery_all_country = $allCountry;
        $listing->save();
        
        return redirect()->route('cgl.listing.photos', [ 'listingId' => $listing->id ]);
    }
}
