<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Listing;
use App\ListingLocation;
use App\Place;

class ListingLocationController extends Controller
{
    private $redirectTo;
    private $countryId;

    public function __construct() {
        $this->redirectTo = 'cgl.dashboard';
        $this->countryId = 10;
    }

	public function show($id)
	{
        if (empty($id))
            return redirect()->route($this->redirectTo);
            
        $listing = Listing::where(['id' => $id, 'user_id' => Auth::id()])->first();
        
        if (! $listing)
            return redirect()->route($this->redirectTo);

        $location = ListingLocation::where('listing_id', $id)->first();
            
        $listingId = $id;
        $places = Place::where('parent_id', $this->countryId)->orderBy('name', 'asc')->get();
        
        return view(
            'admin.listing.location',
            compact([
                'listingId',
                'location',
                'places',
            ])
        );
    }

	public function handle($id, Request $request)
	{
        if (empty($id))
            return redirect()->route($this->redirectTo);
            
        $listing = Listing::where(['id' => $id, 'user_id' => Auth::id()])->first();

        if (! $listing)
            return redirect()->route($this->redirectTo);
            
        $rules = [
            'address' => 'required|string',
            'place_level_2' => 'required|numeric',
            'place_level_3' => 'required|numeric',
            'place_level_4' => 'required|numeric',
        ];
        
        $data = $request->validate($rules);

        $location = ListingLocation::find($listing->id);

        if (! $location)
        {
            ListingLocation::create([
                'listing_id' 		=> $listing->id,
                'address' 		    => $request->input('address'),
                'place_level_2' 	=> $request->input('place_level_2'),
                'place_level_3' 	=> $request->input('place_level_3'),
                'place_level_4' 	=> $request->input('place_level_4'),
            ]);
        }
        else
        {
            ListingLocation::where('listing_id', $listing->id)->update([
                'address'       => $request->input('address'),
                'place_level_2' => $request->input('place_level_2'),
                'place_level_3' => $request->input('place_level_3'),
                'place_level_4' => $request->input('place_level_4'),
            ]);
        }

        return redirect()->route('cgl.listing.delivery', [ 'id' => $listing->id]);
    }

}
