<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\UserDetail;
use App\TypeUser;
use App\StatusUser;

class ProfileController extends Controller
{
		public function show()
		{
				$isAdminProfile = TRUE;

				$user = Auth::user();
				$utyp = TypeUser::find($user->type_id);
				$usta = StatusUser::find($user->status_id);

				$udet = UserDetail::find($user->id);

				if (!$udet)
				{
						$udet = new UserDetail;
						$udet->user_id = $user->id;
						$udet->description = '';
						$udet->save();
				}

				return view('admin.profile',
						compact('user', 'udet', 'utyp', 'usta', 'isAdminProfile')
				);
		}

		public function handle(Request $request)
		{
				$rules = [
						'name'      		=> 'required|string',
						'phone'     		=> 'required|string',
						'description' 	=> 'nullable|string',
						'sn_whatsapp' 	=> 'nullable|string',
						'sn_skype' 			=> 'nullable|string',
						'sn_facebook' 	=> 'nullable|url',
						'sn_instagram' 	=> 'nullable|url',
						'sn_twitter' 		=> 'nullable|url',
						'sn_youtube' 		=> 'nullable|url',
						'sn_pinterest' 	=> 'nullable|url',
				];
				
				$this->validate($request, $rules);

				$phone = str_replace(' ', '', $request->input('phone'));
				$whatsapp = str_replace(' ', '', $request->input('sn_whatsapp'));
				
				$userId = Auth::id();
				
				User::where('id', $userId)
				->update([
						'name' 	=> $request->input('name'),
						'phone' => $phone,
				]);

				$userDetail = UserDetail::find($userId);

				if (!$userDetail)
				{
						UserDetail::create([
								'user_id' 			=> $userId,
								'whatsapp' 			=> $whatsapp,
								'skype' 				=> $request->input('sn_skype'),
								'facebook' 			=> $request->input('sn_facebook'),
								'instagram' 		=> $request->input('sn_instagram'),
								'twitter' 			=> $request->input('sn_twitter'),
								'youtube' 			=> $request->input('sn_youtube'),
								'pinterest' 		=> $request->input('sn_pinterest'),
								'description' 	=> $request->input('description'),
						]);
				}
				else
				{
						UserDetail::where('user_id', $userId)
						->update([
								'whatsapp' 			=> $whatsapp,
								'skype' 				=> $request->input('sn_skype'),
								'facebook' 			=> $request->input('sn_facebook'),
								'instagram' 		=> $request->input('sn_instagram'),
								'twitter' 			=> $request->input('sn_twitter'),
								'youtube' 			=> $request->input('sn_youtube'),
								'pinterest' 		=> $request->input('sn_pinterest'),
								'description' 	=> $request->input('description'),
						]);
				}

				$resp = [
						'success'   => true,
						'message' 	=> 'Datos actualizados correctamente!',
				];

				return $resp;
		}
}
