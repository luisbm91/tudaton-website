<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\TypeUser;
use App\StatusUser;
use App\Store;

class StoreController extends Controller
{
		public function show()
		{
				$isAdminStore = TRUE;

				$user = Auth::user();

				if ($user->type_id == 2)
				{
						return redirect()->route('cgl.dashboard');
				}

				$store = Store::find($user->id);

				if (!$store)
				{
						$store = new Store;
						$store->user_id = $user->id;
						$store->name = '';
						$store->save();
				}

				$utyp = TypeUser::find($user->type_id);
				$usta = StatusUser::find($user->status_id);
				
				return view('admin.store',
						compact('user', 'utyp', 'usta', 'store', 'isAdminStore')
				);
		}

		public function handle(Request $request)
		{
				$rules = [
						'name' 		=> 'required|string',
						'address' => 'required|string',
				];
				
				$this->validate($request, $rules);

				$userId = Auth::id();
				
				Store::where('user_id', $userId)
				->update([
						'name' 		=> $request->input('name'),
						'address' => $request->input('address'),
				]);

				$resp = [
						'success'   => true,
						'message' 	=> 'Datos actualizados correctamente!',
				];

				return $resp;
		}

		public function deal()
		{
        $user = Auth::user();
        $utyp = TypeUser::find($user->type_id);
        $usta = StatusUser::find($user->status_id);

				User::where('id', $user->id)
				->update([
						'type_id' => 3,
				]);
				
				return redirect()->route('cgl.store');
		}
}
