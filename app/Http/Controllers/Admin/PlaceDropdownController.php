<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Place;

class PlaceDropdownController extends Controller
{

	public function handle(Request $request)
	{
        $parentId = intval($request->input('id'));

        if (empty($parentId))
            return;
            
        $places = Place::where('parent_id', $parentId)->orderBy('name', 'asc')->get();
        
        $result = '<option value="">--</option>';
        foreach ($places as $row)
        {
            $result .= '<option value="'.$row->id.'">'.$row->name.'</option>';
        }

        return $result;
    }

}
