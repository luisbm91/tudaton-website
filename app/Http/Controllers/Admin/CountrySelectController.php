<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Country;
use App\User;

class CountrySelectController extends Controller
{
    public function show()
    {
        $user = Auth::user();

        if ($user->country_id)
            return redirect()->route('cgl.dashboard');

        $countries = Country::orderBy('name', 'asc')->get();
        
    	return view('admin.country.select', compact('countries'));
    }

    public function handle(Request $request)
    {
        $rules = [
            'country' => 'required|numeric',
            'number' => 'required|numeric',
        ];

        $messages = [
            'country.required' => 'Por favor, selecciona tu Pais',
        ];

        $this->validate($request, $rules, $messages);

        $userId = Auth::id();

        User::where('id', $userId)
        ->update(['country_id' => $request->input('country')]);

        if ($request->input('number') == 1)
            return redirect()->route('cgl.listing.create');
        else
            return redirect()->route('cgl.dashboard');
    }
}
