<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Listing;
use App\User;
use App\UserDetail;
use App\Currency;
use App\Photo;

class ListingDetailController extends Controller
{
	public function show($id)
	{
        $listing = Listing::where('id', $id)
            ->select('id', 'title', 'currency_id', 'price', 'category_id', 'user_id')
            ->first();
                
        if (!$listing)
            return redirect()->route('home');
            
        $currency = Currency::where('id', $listing->currency_id)
            ->select('name')
            ->first();

        $user = User::where('id', $listing->user_id)
            ->select('name', 'phone', 'image')
            ->first();

        $userDetail = UserDetail::where('user_id', $listing->user_id)
            ->select('whatsapp', 'skype', 'facebook', 'instagram', 'twitter', 'youtube', 'pinterest')
            ->first();

        if (empty($user->image)) {
            $avatarUrl = url('img/avatar.jpg');
        }
        else {
            $avatarUrl = Storage::url(sha1($listing->user_id).'/'.$user->image);
        }
        
        $photos = Photo::where('listing_id', $listing->id)
                ->select('name')
                ->get();

        $images = array();

        if (!$photos)
        {
            $images[0] = url('img/no-image.jpg');
        }
        else
        {
            foreach ($photos as $key => $val)
            {
                $images[$key] = Storage::url(sha1($listing->user_id).'/'.$val->name);
            }
        }
        
        return view('listing_detail', 
            compact(['listing', 'currency', 'images', 'user', 'userDetail'])
        );
	}
}
