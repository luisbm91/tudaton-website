<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function show()
    {
        return view('auth.login');
    }

    public function handle(Request $request)
    {
        $rules = [
            'email'     => 'required|string|email|max:100',
            'password'  => 'required|string|min:8',
        ];

        $this->validate($request, $rules);
        
        $credentials = [
          'email'     => $request->input('email'),
          'password'  => $request->input('password'),
        ];

        if (Auth::attempt($credentials))
        {
            $resp = [
                'success'   => true,
                'url'       => route('cgl.dashboard'),
            ];

            return $resp;
        }

        $resp = [
            'success'   => false,
            'message'   => 'Credenciales inválidas',
        ];

        return $resp;
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('login');
    }

}
