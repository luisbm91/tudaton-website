<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use Socialite;

class SocialLoginFacebookController extends Controller
{

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        try {
            $socialUser = Socialite::driver('facebook')->user();

            $user = User::where('facebook_id', $socialUser->id)->first();

            if ($user)
            {
                Auth::login($user);
                return redirect()->route('user.dashboard');
            }
            else
            {
                $user = User::where('email', $socialUser->email)->first();

                if ($user)
                {
                    User::where('id', $user->id)
                        ->update([
                            'facebook_id' => $socialUser->id
                        ]);

                    Auth::login($user);
                    return redirect()->route('user.dashboard');
                }
                else
                {
                    $newUser = User::create([
                        'name'          => $socialUser->name,
                        'email'         => $socialUser->email,
                        'facebook_id'   => $socialUser->id,
                        'password'      => Hash::make(rand(5, 15))
                    ]);

                    Auth::login($newUser);
                    return redirect()->back();
                }
            }
        }
        catch(Exception $e) {
            return redirect()->route('login');
        }
    }
}
