<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

class RegisterController extends Controller
{
    public function show()
    {
    	return view('auth.register');
    }

    public function handle(Request $request)
    {
        $rules = [
            'name'      => 'required|string|max:100',
            'email'     => 'required|string|email|max:100|unique:users|email:rfc,dns',
            'password'  => 'required|string|min:8|confirmed',
        ];

        $messages = [
            'email.unique' => 'El :attribute ingresado ya está registrado.',
        ];

        $this->validate($request, $rules, $messages);

        User::create([
            'name'      => $request->input('name'),
            'email'     => $request->input('email'),
            'password'  => Hash::make($request->input('password')),
            'type_id'   => 2,
            'status_id' => 1,
        ]);

        $credentials = [
            'email'     => $request->input('email'),
            'password'  => $request->input('password'),
        ];

        if (Auth::attempt($credentials))
        {
            $resp = [
                'success'   => true,
                'url'       => route('cgl.country.select'),
            ];

            return $resp;
        }

        $resp = [
            'success'   => false,
            'message'   => 'Credenciales inválidas',
        ];

        return $resp;
    }
}
