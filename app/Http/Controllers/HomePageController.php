<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Country;
use App\Listing;
use App\Currency;
use App\Category;
use App\Photo;
use App\Store;

class HomePageController extends Controller
{
	public function show()
	{
        $isPageHome = TRUE;
        $clientIp   = $_SERVER['REMOTE_ADDR'];
        // $clientIp = '174.130.203.74';

        $data = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$clientIp));
        $countryCode = 'PE';

        if (!empty($data->geoplugin_countryCode)) {
            $countryCode = $data->geoplugin_countryCode;
        }

        $country = Country::where('code', $countryCode)
                    ->select('id', 'name')
                    ->first();
                    
        if ($country == null)
        {
            $country = Country::where('code', 'PE')
                    ->select('id', 'name')
                    ->first();
        }
                    
        $countries = Country::select('id', 'name', 'code')
                    ->orderBy('name', 'asc')
                    ->get();

        $listings = Listing::where('status_id', 2)
                    ->select('id', 'title', 'price', 'currency_id',
                    'category_id', 'published_at', 'user_id')
                    ->orderBy('id', 'desc')
                    ->limit(8)
                    ->get();
                    
        $arrListings = array();
        foreach ($listings as $key => $val)
        {
            $currn = Currency::where('id', $val->currency_id)
                    ->select('name')->first();

            $categ = Category::where('id', $val->category_id)
                    ->select('name')->first();

            $photo = Photo::where('listing_id', $val->id)
                    ->select('name')->limit(1)->first();

            if (isset($photo->name)) {
                $image = Storage::url(sha1($val->user_id).'/'.$photo->name);
            }
            else {
                $image = url('img/no-image.jpg');
            }

            $date = date('d/m/Y', strtotime($val->published_at));
            
            $arrListings[$key] = [
                'id'        => $val->id,
                'title'     => strtoupper($val->title),
                'price'     => number_format(round($val->price, 2)),
                'image'     => $image,
                'currency'  => $currn->name,
                'category'  => $categ->name,
                'date'      => $date,
            ];
        }

        $stores = Store::all();
        
        return view('pages.home',
            compact('isPageHome', 'countries', 'country', 'arrListings', 'stores')
        );
	}
    
}
