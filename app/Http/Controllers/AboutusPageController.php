<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AboutusPageController extends Controller
{
		public function show()
		{
				$isPageAboutus = TRUE;
				return view('pages.aboutus', compact('isPageAboutus'));
		}
}
