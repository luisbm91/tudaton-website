<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingDescription extends Model
{

    protected $table = 'public.listing_descriptions';

    protected $primaryKey = 'listing_id';
    
    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'listing_id', 'description'
    ];

}
