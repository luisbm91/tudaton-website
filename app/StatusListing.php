<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusListing extends Model
{
  protected $table = 'public.statuses_listings';
  
  public $incrementing = false;

  public $timestamps = false;
}
